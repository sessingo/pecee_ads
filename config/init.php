<?php
// Defines include paths
$abspath = substr(__FILE__,0,strlen(__FILE__) - strlen('config/init.php'));
set_include_path($abspath . PATH_SEPARATOR . $abspath . 'lib' . DIRECTORY_SEPARATOR);
include 'config/config.php';
include LibraryPath . '/config/routes.php';
