<?php
class Helper {
	public static function WriteAd(Model_Ad $ad) {
		if(!$ad->getIsHtml()) {
			$tmp = explode('.', $ad->getContent());
			$fileExt = $tmp[count($tmp)-1];
			switch(strtolower($fileExt)) {
				default:
					$output = '<img src="'.$ad->getContent().'" border="0" style="width:'.$ad->getWidth().'px;height:'.$ad->getHeight().'px;" />';
					break;
				case 'swf':
					// TODO implement swfobject.
					break;
			}
			return '<a href="' . Controller_Ads::MakeJavascriptClickUri($ad->getAdID()) . '/" target="_blank">'. $output .'</a>';
		} else {
			return preg_replace("%\n|\r%", " ", $ad->getContent());
		}
	}
}