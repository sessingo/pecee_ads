<?php
class Widget_Ads_Statistic extends Widget_Site {
	protected $ad;
	protected $hoursAgo;
	protected $statistic;
	protected $weekStatistic;
	protected $weekStatisticStartFrom;
	
	public function __construct($adId) {
		$this->navActiveIndex=1;
		parent::__construct();
		$userId = ((\Pecee\Model\User\ModelUser::Current()->getAdminLevel() > 1) ? NULL : \Pecee\Model\User\ModelUser::Current()->getUserID());
		$this->ad = Model_Ad::GetById($adId);
		if(!$this->ad->hasRow()) {
			\Pecee\Router::GetRoute('ad', '');
		}
		/*$this->hoursAgo = $this->getParam('HoursAgo', 24);
		$this->statistic = DB_Ads::GetStatisticsByHoursAgo($adId, $this->hoursAgo, $adId);
		
		$this->weekStatisticStartFrom = $this->getParam('WeekStartFrom', 0);
		$this->weekStatistic = DB_Ads::GetWeekStatisticsByAdID($adId, $adId, $this->weekStatisticStartFrom );*/
	}
}