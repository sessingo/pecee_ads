<?php /* @var $this Widget_Users_New */ ?>
<h3><?= $this->_('Ny bruger')?></h3>
<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP) ?>

<?= $this->form()->start('newUser')?>
<?= $this->showErrors(); ?>

<table cellspacing="0" cellpadding="0" width="100%" class="margin-top">
	<tr>
		<td width="30%">
			<?= $this->_('Brugernavn') ?>
		</td>
		<td>
			<?= $this->form()->input('username', 'text'); ?>
		</td>
	</tr>
	<tr>
		<td width="30%" class="padding-top">
			<?= $this->_('Adgangskode') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->input('password', 'password'); ?>
		</td>
	</tr>
	<tr>
		<td width="30%" class="padding-top">
			<?= $this->_('Gentag adgangskode') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->input('passwordRepeat', 'password'); ?>
		</td>
	</tr>
	<tr>
		<td width="30%" class="padding-top">
			<?= $this->_('E-mail') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->input('email', 'text'); ?>
		</td>
	</tr>
	<tr>
		<td width="30%" class="padding-top">
			<?= $this->_('Bruger type') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->selectStart('UserAdminLevel', new Dataset_User_AdminLevel())?>
		</td>
	</tr>
	<tr>
		<td width="30%">
		</td>
		<td class="padding-top">
			<?= $this->form()->submit('submit', $this->_('Gem'))->addAttribute('class', 'margin-top'); ?>
		</td>
	</tr>
</table>
<?= $this->form()->end();?>