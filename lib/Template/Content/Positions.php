<?php /* @var $this Widget_Positions */ ?>
<h3><?= $this->_('Positions')?></h3>
<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP) ?>
<? if($this->positions->hasRows()) : ?>
	<table cellspacing="0" width="100%" cellpadding="0" class="margin-top">
		<tr>
			<td width="20%" class="bold padding-bottom">
				<?= $this->_('Navn')?>
			</td>
			<td width="20%" class="bold padding-bottom">
				<?= $this->_('Type')?>
			</td>
			<td width="20%" class="bold padding-bottom">
				<?= $this->_('Unikt tag')?>
			</td>
			<td width="15%" class="bold padding-bottom">
				<?= $this->_('Side') ?>
			</td>
			<td width="10%" class="bold padding-bottom" align="center">
				<?= $this->_('Aktiv') ?>
			</td>
			<td width="20%" class="bold padding-bottom" align="right">
				<?= $this->_('Funktioner') ?>
			</td>
		</tr>	
	<? foreach($this->positions->getRows() as $position) : ?>
		<tr> 
			<td height="25">
				<?= $position->getName(); ?>
			</td>
			<td>
				<?= $position->getWidth() ?>x<?= $position->getHeight() ?>
			</td>
			<td>
				<?= $position->getUniqueTag(); ?>
			</td>
			<td>
				<?= $position->getSiteName(); ?>
			</td>
			<td align="center">
				<?= $this->form()->bool('active[]', (!$position->getDisabled())); ?>
			</td>
			<td align="right">
				<a href="<?= \Pecee\Router::GetRoute('positions', 'edit', array($position->getPositionID())); ?>"><?= $this->_('Rediger') ?></a>
				<span class="separator">|</span>
				<a onclick="return confirm('<?= $this->_('Er du sikker på, at du vil slette denne position?');?>');" href="<?= \Pecee\Router::GetRoute('positions', 'delete', array($position->getPositionID())); ?>"><?= $this->_('Slet')?></a>
			</td>
		</tr>
	<? endforeach; ?>
	</table>
<? endif; ?>