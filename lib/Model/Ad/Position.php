<?php
class Model_Ad_Position extends \Pecee\Model\Model {
	protected $type;
	protected $rules;
	public function __construct() {
		parent::__construct('Position', array('PositionID' => NULL, 'SiteID' => '', 'TypeID' => '', 'UniqueTag' => '',
												'Name' => '', 'Visible' => NULL, 'Disabled' => NULL));
	}
	
	public function getSite() {
		return Model_Ad_Site::GetById($this->SiteID);
	}
	
	public function clearCache() {
		Site::Instance()->cache->clear(Site_Cache::GetNamespace($this->SiteID));
	}
	
	public function save() {
		$this->PositionID=parent::save()->getInsertId();
		$this->saveRules();
		$this->clearCache();
	}
	
	public function update() {
		Model_Ad_Position_Rule::Clear($this->PositionID);
		$this->saveRules();
		parent::update();
		$this->clearCache();
	}
	
	public function delete() {
		// TODO: REMOVE CACHE HERE !!!
		parent::delete();
	}
	
	protected function saveRules() {
		if(is_array($this->rules)) {
			foreach($this->rules as $rule) {
				$rule->setPositionID($this->PositionID);
				$rule->save();
			}
		}
	}
	
	public function setRules(array $rules) {
		$this->rules=$rules;
	}
	
	public function getRules() {
		return Model_Ad_Position_Rule::GetByPositionId($this->PositionID);
	}
	
	public function getTemplate() {
		return Model_Ad_Template::GetByPositionId($this->PositionID);
	}
	
	/**
	 * Get type
	 * @return Model_Ad_Type
	 */
	public function getType() {
		if(!$this->type) {
			$this->type=Model_Ad_Type::GetById($this->TypeID);
		}
		return $this->type;
	}
	
	public static function Get($query=NULL, $rows=30, $page=0) {
		$where = array('1=1');
		if($query) {
			$where = '`UniqueTag` LIKE \'%'.\Pecee\DB\DB::Escape($query).'%\' OR `Name` LIKE \''.\Pecee\DB\DB::Escape($query).'\'';
		}
		return self::FetchPage('P.`PositionID`', 'SELECT P.*, S.`Name` AS "SiteName", T.`Height`, T.`Width` FROM `Position` P
									LEFT JOIN `Site` S ON(S.`SiteID` = P.`SiteID`) LEFT JOIN `Type` T ON(T.`TypeID` = P.`TypeID`) 
									WHERE '.join(' AND ', $where).'  ORDER BY S.`Name` ASC, P.`PositionID` DESC');
	}
	
	/**
	 * Get position by position id
	 * @param int $positionId
	 * @return Model_Ad_Position
	 */
	public static function GetById($positionId) {
		return self::FetchOne('SELECT * FROM Position WHERE PositionID = %s', $positionId);
	}
	
	public static function GetBySiteId($siteId=NULL, $visible=NULL) {
		$where=array('1=1');
		if(!is_null($siteId)) {
			$where[] = \Pecee\DB\DB::FormatQuery('SiteID = %s', array($siteId));
		}
		if(!is_null($visible)) {
			$where[] = \Pecee\DB\DB::FormatQuery('Visible = %s', array($visible));
		}
		return self::FetchAll('SELECT * FROM Position WHERE '. join(' AND ', $where));
	}
}