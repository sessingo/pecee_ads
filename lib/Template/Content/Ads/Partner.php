<? /* @var $this Widget_Ads_Partner */ ?>
<div style="position:relative;">
	<div style="position:absolute;top:0px;right:0px;">
		<?= $this->form()->start('search', 'get')?>
		<?= $this->form()->input('SiteID', 'hidden', $this->siteId) ?>
		<?= $this->form()->input('Query', 'text')?>&nbsp;
		<?= $this->form()->submit('search', $this->_('Søg'))?>
		<?= $this->form()->end(); ?>
	</div>
	<h3><?= $this->_('Dine reklamer')?></h3>
</div>
<div style="position:relative;">
	<div style="position:absolute;top:0px;right:0px;">
		<span style="font-size:11px;"><?= $this->_('Filtrer efter side'); ?></span>&nbsp;
		<?= $this->form()->selectStart('filter', new Dataset_Sites($this->_('-- Vælg --')), $this->siteId)
			->addAttribute('onchange', 'top.location.href = \'?SiteID=\' + this.value;') ?>
	</div>
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP) ?>
</div>
<? 
	if($this->ads->hasRows()) { ?>
	<table cellspacing="0" width="100%" cellpadding="0" class="margin-top">
		<tr>
			<td width="20%" class="bold padding-bottom">
				<?= $this->_('Name')?>
			</td>
			<td width="8%" align="center" class="bold padding-bottom">
				<?= $this->_('Site') ?>
			</td>
			<td width="20%" align="center" class="bold padding-bottom">
				<?= $this->_('Position')?>
			</td>
			<!-- <td width="8%" align="center" class="bold padding-bottom">
				<?= $this->_('Type') ?>
			</td>  -->
			<td width="8%" align="center" class="bold padding-bottom">
				%
			</td>
			<td width="8%" align="center" class="bold padding-bottom">
				Active
			</td>
			<td width="23%" class="bold padding-bottom" align="right">
				<?= $this->_('Funktioner') ?>
			</td>
		</tr>	
	<? foreach($this->ads->getRows() as $ad) : ?>
		<tr> 
			<td class="padding-bottom">
				<?= $ad->Name; ?>
			</td>
			<td align="center" height="25" class="padding-bottom">	
				<a href="#something=<?= $ad->siteId ?>"><?= $ad->SiteName ?></a>
			</td>
			<td align="center" class="padding-bottom">
				<?= $ad->PositionName; ?>
			</td>
			<!-- <td align="center" height="25" class="padding-bottom">
				<?= $ad->TypeWidth; ?>x<?= $ad->TypeHeight; ?>
			</td> -->
			<td align="center" class="padding-bottom">
				<?= $ad->ShowPercentage?>%
			</td>
			<td align="center" class="padding-bottom">
				<?= $this->form()->bool('active_' . $ad->AdID, !($ad->getDisabled()))->addAttribute('disabled', 'true')?>
			</td>
			<td align="right" class="padding-bottom">
				<a href="<?= \Pecee\Router::GetRoute('ads', 'statistic', array($ad->AdID)) ?>"><?= $this->_('Statistik')?></a>
			</td>
		</tr>
		<?php endforeach; ?>
		<tr>
			<td colspan="7" width="100%" align="center" class="padding-top">
				<div class="padding-top margin-top large">
					<?= \Pecee\UI\Paging::DisplayNumberedPaging(6, $this->page, $this->ads->getMaxPages(), '?Page=%1$d', $this->_('Forrige'), $this->_('Næste'))?>
				</div>
			</td>
		</tr>
		</table>
	<?php
	}

?>