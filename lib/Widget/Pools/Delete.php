<?php
class Widget_Pools_Delete extends Widget_Site {
	public function __construct($poolId) {
		$this->navActiveIndex=3;
		parent::__construct();
		$pool=Model_Pool::GetById($poolId);
		if($pool->hasRow()) {
			$pool->delete();
		}
		\Pecee\Router::GoBack();
	}
}