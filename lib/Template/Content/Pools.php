<? /* @var $this Widget_Pools */ ?>
<div style="position:relative;">
	<div style="position:absolute;top:0px;right:0px;text-align:right;">
		<?= $this->form()->start('search', 'get')->addAttribute('style','padding-bottom:10px;')?>
		<?= $this->form()->input('SiteID', 'hidden', $this->siteId) ?>
		<?= $this->form()->input('Query', 'text', $this->query)?>&nbsp;
		<?= $this->form()->submit('search', $this->_('Søg'))?>
		<?= $this->form()->end(); ?>
		
		<span style="font-size:11px;"><?= $this->_('Filtrer efter side'); ?></span>&nbsp;
		<?= $this->form()->selectStart('filter', new Dataset_Sites($this->_('Alle')), $this->siteId)
			->addAttribute('onchange', 'top.location.href = \'?siteId=\' + this.value;') ?>
		
	</div>
	<h3><?= $this->_('Pools')?></h3>
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP) ?>
</div>

<? if($this->pools->hasRows()) : ?>
	<table cellspacing="0" width="100%" cellpadding="0" class="margin-top">
		<tr>
			<td width="20%" class="bold padding-bottom">
				<?= $this->_('ID')?>
			</td>
			<td width="20%" class="bold padding-bottom">
				<?= $this->_('Navn')?>
			</td>
			<td width="20%" class="bold padding-bottom">
				<?= $this->_('Beskrivelse')?>
			</td>
			<td width="20%" class="bold padding-bottom">
				<?= $this->_('Størrelse')?>
			</td>
			<td width="15%" class="bold padding-bottom">
				<?= $this->_('Nedarver fra') ?>
			</td>
			<td width="10%" class="bold padding-bottom" align="center">
				<?= $this->_('Aktiv') ?>
			</td>
			<td width="20%" class="bold padding-bottom" align="right">
				<?= $this->_('Funktioner') ?>
			</td>
		</tr>	
	<? /* @var $this Model_Pool */
	foreach($this->pools->getRows() as $pool) : ?>
		<tr> 
			<td height="25">
				<code><?= $pool->getPoolID(); ?></code>
			</td>
			<td>
				<?= $pool->getName(); ?>
			</td>
			<td>
				<?= $pool->getDescription(); ?>
			</td>
			<td>
				<?= $pool->getType()->getSize(); ?>
			</td>
			<td>
				[parent]
			</td>
			<td align="center">
				<?= $this->form()->bool('active[]', (!$pool->getDisabled()))->addAttribute('disabled','disabled'); ?>
			</td>
			<td align="right">
				<a href="<?= \Pecee\Router::GetRoute(NULL, 'edit', array($pool->getPoolID())); ?>"><?= $this->_('Rediger') ?></a>
				<span class="separator">|</span>
				<a onclick="return confirm('<?= $this->_('Er du sikker på, at du vil slette denne pool?');?>');" href="<?= \Pecee\Router::GetRoute(NULL, 'delete', array($pool->getPoolID())); ?>"><?= $this->_('Slet')?></a>
			</td>
		</tr>
	<? endforeach; ?>
	</table>
<? else: ?>
	<div style="text-align:center;" class="bold large padding-top margin-top">
		<?= $this->_('Der er endnu ikke tilføjet nogle pools')?>
	</div>
<? endif;?>

