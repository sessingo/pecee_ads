<?php
class Controller_Templates extends \Pecee\Controller\Controller {

	private function ShowMenu() {
		$menu = new \Pecee\UI\Menu\Menu();
		$menu->setClass('menu');	
		$menu->addItem($this->_('Ny template'), \Pecee\Router::GetRoute('templates', 'new'));
		$menu->addItem($this->_('Oversigt'), \Pecee\Router::GetRoute('templates', ''));
		\Pecee\UI\Site::GetInstance()->addToLocation($menu, \Pecee\UI\Site::LOCATION_TOP);
	}
	
	public function indexView() {
		$this->ShowMenu();
		$widget = new Widget_Templates();
		echo $widget;
	}
	
	public function newView( $SiteID = NULL ) {
		$this->ShowMenu();
		$widget = new Widget_Templates_New( $SiteID );
		echo $widget;
	}
	
	public function editView( $TemplateID, $SiteID = NULL ) {
		$this->ShowMenu();
		$widget = new Widget_Templates_Edit( $TemplateID, $SiteID );
		echo $widget;
	}
	
	public function deleteView($templateId) {
		$widget = new Widget_Templates_Delete($templateId);
		echo $widget;
	}
	
	public function previewView() {
		$widget = new Widget_Templates_Preview();
		echo $widget;
	}
	
}