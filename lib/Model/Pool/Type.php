<?php
class Model_Pool_Type extends \Pecee\Model\Model {
	public function __construct() {
		parent::__construct('PoolType', array('PoolTypeID' => NULL, 'Width' => NULL, 'Height' => NULL));
	}
	
	public function getSize() {
		return sprintf('%sx%s', $this->Width, $this->Height);
	}
	
	public function save() {
		$this->PoolTypeID=parent::save()->getInsertId();
	}
	
	/**
	 * Get type by id
	 * @param string $TypeId
	 * @return Model_Pool_Type
	 */
	public static function GetById($poolTypeId) {
		return self::FetchOne('SELECT * FROM `PoolType` WHERE `PoolTypeID` = %s', $poolTypeId);
	}
	
	/**
	 * Get all ads or filter by width, height or both.
	 * @param int|null $width
	 * @param int|null $height
	 * @param int|null $rows
	 * @param int|null $page
	 * @return Model_Pool_Type
	 */
	public static function Get($width=NULL, $height=NULL, $rows=25, $page=0) {
		$where = array('1=1');
		if(!is_null($width) || !is_null($width)) {
			$where[] = Pecee\DB\DB::FormatQuery('`Width` LIKE %s OR `Height` LIKE %s', array($width, $height));
		}
		return self::FetchPage('`PoolTypeID`', 'SELECT * FROM `PoolType` WHERE ' . join(' AND ', $where), $rows, $page);
	}
}