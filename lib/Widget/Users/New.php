<?php
class Widget_Users_New extends Widget_Site {
	public function __construct() {
		$this->navActiveIndex=7;
		parent::__construct();	
		$this->addInputValidation($this->_('Brugernavn'), 'username', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
		$this->addInputValidation($this->_('Adgangskode'), 'password', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
		$this->addInputValidation($this->_('Gentag adgangskode'), 'passwordRepeat', array(new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty(), new \Pecee\UI\Form\Validate\ValidateInputRepeation($this->_('Adgangskode'), $this->data->password)));
		$this->addInputValidation($this->_('E-mail'), 'email', new \Pecee\UI\Form\Validate\ValidateInputEmail());
		$this->addInputValidation($this->_('Bruger type'), 'UserAdminLevel', array(new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty(), new \Pecee\UI\Form\Validate\ValidateInputInteger()));	
	
		if( $this->isPostBack() && !$this->hasErrors() ) {
			$user=new \Pecee\Model\User\ModelUser($this->data->username, $this->data->password);
			$user->setEmail($this->data->email);
			$user->AdminLevel=$this->data->UserAdminLevel;
			$user->save();
			\Pecee\Router::Redirect(\Pecee\Router::GetRoute('users', ''));
		}
	}
}