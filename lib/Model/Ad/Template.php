<?php
class Model_Ad_Template extends \Pecee\Model\Model {
	const SESSION_ID='Template_Preview';
	public function __construct() {
		parent::__construct('Template', array('TemplateID' => NULL, 'PoolID' => NULL, 'Name' => '', 
										'Content' => '', 'PubDate' => \Pecee\Date::ToDateTime(time()), 'Disabled' => FALSE));
	}
	
	public function getPool() {
		return Model_Pool::GetById($this->PoolID);
	}
	
	/**
	 * Get template by id
	 * @param int $templateId
	 * @return Model_Ad_Template
	 */
	public static function GetById($templateId) {
		return self::FetchOne('SELECT * FROM `Template` WHERE `TemplateID` = %s', $templateId);
	}
	
	/**
	 * Get template by position id
	 * @param int $templateId
	 * @return Model_Ad_Template
	 */
	public static function GetByPoolId($poolId) {
		return self::FetchOne('SELECT t.* FROM `Template` t JOIN `Pool` p ON(p.`PoolID` = t.`PoolID`) JOIN `Site` s ON(s.`SiteID` = p.`SiteID`) WHERE s.`Disabled` = 0 AND t.`PoolID` = %s AND t.`Disabled` = 0', $poolId);
	}
	
	public static function Get($query=NULL, $order=NULL, $rows=15, $page=0) {
		$where = array('1=1');
		$order=(is_null($order)) ? 'TemplateID DESC' : $order;
		if($query) {
			$where[] = 'Name LIKE \'%'.\Pecee\DB\DB::Escape($query).'%\' OR Content LIKE \''.\Pecee\DB\DB::Escape($query).'\'';
		}
		return self::FetchPage('TemplateID', 'SELECT * FROM `Template` WHERE ' . join(' AND ', $where) . ' ORDER BY ' . $order, $rows, $page);
	}
}