<?php
class Widget_Types_Edit extends Widget_Site {
	protected $type;
	public function __construct($typeId) {
		$this->navActiveIndex=4;
		parent::__construct();
		if(\Pecee\Integer::is_int($typeId)) {
			$this->type = Model_Pool_Type::GetById($typeId);
			if(!$this->type->hasRow()){
				\Pecee\Router::Redirect(\Pecee\Router::GetRoute('types', ''));
			}
			$this->addInputValidation($this->_('Bredde'), 'Width', new \Pecee\UI\Form\Validate\ValidateInputInteger());
			$this->addInputValidation($this->_('Højde'), 'Height', new \Pecee\UI\Form\Validate\ValidateInputInteger());
			
			if($this->isPostBack() && !$this->hasErrors()) {
				$oldWidth=$this->type->getWidth();
				$oldHeight=$this->type->getHeight();
				
				$this->type->setWidth($this->data->Width);
				$this->type->setHeight($this->data->Height);
				$this->type->update();
				
				Model_Ad_Log::Log(sprintf('%s opdatede typen %sx%s til %sx%s (%s)',
				\Pecee\Model\User\ModelUser::Current()->getUsername(),
				$oldWidth, $oldHeight,
				$this->data->Width, $this->data->Height,
				$this->type->getPoolTypeID()));
				\Pecee\Router::Redirect(\Pecee\Router::GetRoute('types', ''));
			}
		} else {
			\Pecee\Router::Redirect(\Pecee\Router::GetRoute('types', ''));
		}
	}
}