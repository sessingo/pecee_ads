<?php
class Dataset_Users extends \Pecee\Dataset {
	public function __construct() {
		$users = \Pecee\Model\User\ModelUser::Get();
		if($users->hasRows()) {
			$this->createArray('', $this->_('-- Vælg --'));
			foreach($users->getRows() as $user) {
				$this->createArray($user->UserID, $user->Username);
			}
		}
	}
}