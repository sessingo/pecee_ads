<?php
class Model_Ad_Click extends \Pecee\Model\Model {
	public function __construct() {
		parent::__construct('AdClick', array('AdClickID' => NULL, 'AdID' => NULL, 'IPAddress' => '', 'Date' => \Pecee\Date::ToDateTime(time())));
	}
	
	public static function Find($adId, $ip) {
		return self::FetchOne('SELECT `IPAddress` FROM `AdClick` WHERE `AdID` = %s AND IPAddress = %s', $adId, $ip);
	}
}