<?php
class Widget_Templates_Edit extends Widget_Site {
	protected $template;
	protected $siteId;
	protected $showPreviewButton;
	public function __construct($templateId, $siteId=NULL) {
		$this->navActiveIndex=2;
		parent::__construct();
		if($this->isPostBack()) {
			$this->siteId=$siteId;
			$this->template=\Pecee\String\Encoding::Base64Decode($this->data->template, $this->template);
			$this->template->setName($this->data->Name);
			$this->template->setContent($this->data->Content);
			$this->template->setPoolID($this->data->PoolID);
			if($this->data->previewPostBack) {
				if($this->data->PoolID) {
					$this->showPreviewButton = true;
				}
			} elseif($this->data->autoPostBack == '0') {
				$this->addInputValidation($this->_('Navn'), 'Name', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
				$this->addInputValidation($this->_('Javascript'), 'content', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
			
				if(!$this->hasErrors()) {
					$this->template->update();
					
					// What about loggin? 
					\Pecee\Router::Redirect(\Pecee\Router::GetRoute('templates', ''));
				}
			}
		} else {
			$this->template=Model_Ad_Template::GetById($templateId);
			$this->siteId = $this->template->getPool()->getSiteID();
		}	
	}
}