<?php
class Model_Pool extends \Pecee\Model\Model {
	protected $keywords;
	public function __construct() {
		parent::__construct('Pool', array('PoolID' => NULL, 'ParentPoolID' => NULL, 'PoolTypeID' => NULL, 'SiteID' => NULL, 
										'UserID' => '', 'Name' => '', 'Description' => '', 'RefreshRateMS' => NULL, 
										'Takeover' => NULL, 'UseFrame' => NULL, 'Disabled' => FALSE, 'PubDate' => \Pecee\Date::ToDateTime(),
										'ActiveFrom' => NULL, 'ActiveTo' => NULL));
	}
	
	/**
	 * Get keywords
	 * @return array|null
	 */
	public function getKeywords() {
		if($this->keywords) {
			return $this->keywords;
		}
		$keywords= Model_Pool_Keyword::GetByPoolId($this->PoolID);
		if($keywords->hasRows()) {
			return $keywords->getRows();
		}
		return NULL;
	}
	
	public function setKeywords($keywords) {
		$this->keywords=$keywords;
	}
	
	public function getTakeover() {
		if($this->Takeover) {
			return TRUE;
		}
		return FALSE;
	}
	
	public function getTemplate() {
		return Model_Ad_Template::GetByPoolId($this->PoolID);
	}
	
	protected function savePools() {
		Model_Pool_Keyword::Clear($this->PoolID);
		if($this->keywords) {
			foreach($this->keywords as $word) {
				$keyword=new Model_Pool_Keyword();
				$keyword->setKeyword($word);
				$keyword->setPoolID($this->PoolID);
				$keyword->save();
			}
		}
	}
	
	public function save() {
		$this->PoolID=parent::save()->getInsertId();
		$this->savePools();
	}
	
	public function delete() {
		Model_Pool_Keyword::Clear($this->PoolID);
		return parent::delete();
	}
	
	public function update() {
		parent::update();
		$this->savePools();
	}
	
	public function getType() {
		return Model_Pool_Type::GetById($this->PoolTypeID);
	}
	
	/**
	 * Get pools
	 * @param string|null $query
	 * @param int|null $siteId
	 * @param int|null $rows
	 * @param int|null $page
	 * @return Model_Pool
	 */	
	public static function Get($query=NULL, $siteId=NULL, $rows=30, $page=0) {
		$where = array('1=1');
		if(!is_null($siteId)) {
			$where[]=\Pecee\DB\DB::FormatQuery('`SiteID` = %s',array($siteId));
		}
		if($query) {
			$where[] = '`Name` LIKE \'%'.\Pecee\DB\DB::Escape($query).'%\' OR `Description` LIKE \''.\Pecee\DB\DB::Escape($query).'\'' . join(' AND ', $where);
		}
		return self::FetchPage('`PoolID`', 'SELECT * FROM `Pool` WHERE ' . join(' AND ', $where) . ' ORDER BY `Name` ASC', $rows, $page);
	}
	
	/**
	 * Get pool for display in ad
	 * @param int $siteId
	 * @param array|null $poolIds
	 * @param array|null $poolKeywords
	 * @return Model_Pool
	 */
	public static function GetAdPool($siteId, $poolIds, $poolKeywords=NULL) {
		$where='';
		if(!is_null($poolKeywords)) {
			$where=\Pecee\DB\DB::FormatQuery('OR pk.`Keyword` IN('.\Pecee\DB\DB::JoinArray($poolKeywords).') AND p.`Disabled` = 0 AND p.`SiteID` = %s AND p.PoolTypeID = 
				(SELECT p.PoolTypeID FROM Pool p JOIN PoolType pt ON(pt.`PoolTypeID` = p.`PoolTypeID`) 
					WHERE `ParentPoolID` = 0 AND `PoolID` IN ('.\Pecee\DB\DB::JoinArray($poolIds).') AND `SiteID` = %s LIMIT 1) 
					AND (p.`ActiveFrom` IS NULL OR p.`ActiveFrom` < %s)
					AND (p.`ActiveTo` IS NULL OR p.`ActiveTo` > %s)
					ORDER BY pk.`PoolKeywordID` DESC, p.`ParentPoolID` DESC, p.`PoolID` DESC', array($siteId, $siteId, \Pecee\Date::ToDateTime(time()), \Pecee\Date::ToDateTime(time())));
		}
		

		return self::FetchAll('SELECT p.* FROM `Pool` p LEFT OUTER JOIN `PoolKeyword` pk ON(pk.`PoolID` = p.`PoolID`)
			WHERE p.`PoolID` IN('. \Pecee\DB\DB::JoinArray($poolIds) .') AND p.`Disabled` = 0 AND p.`SiteID` = %s  AND p.`Disabled` = 0 AND p.`SiteID` = %s ' . $where, 
				$siteId, $siteId, $siteId);
	}
	
	public static function GetBySiteId($siteId) {
		return self::FetchAll('SELECT * FROM `Pool` WHERE `SiteID` = %s', $siteId);
	}
	
	/**
	 * Get pool by pool id.
	 * @param int $poolId
	 * @return Model_Pool
	 */
	public static function GetById($poolId) {
		return self::FetchOne('SELECT * FROM `Pool` WHERE `PoolID` = %s', $poolId);
	}
	
	/**
	 * Get ad pool by id
	 * @param int $adId
	 * @return Model_Ad_Pool
	 */
	public static function GetByAdId($adId) {
		return self::FetchAll('SELECT p.* FROM `Pool` p JOIN `AdPool` ap ON(ap.`PoolID` = p.`PoolID`) WHERE ap.`AdID` = %s AND p.`Disabled` = 0', $adId);
	}
}