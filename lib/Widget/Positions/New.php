<?php
class Widget_Positions_New extends Widget_Site {
	protected $rules;
	protected $sessionId;
	public function __construct() {
		$this->navActiveIndex=3;
		parent::__construct();
		$this->rules=array();
		if($this->isPostBack()) {
			$this->rules=\Pecee\String\Encoding::Base64Decode($this->data->data,array());
			if($this->data->addNewRule) {
				$this->addInputValidation($this->_('URL'), 'Rule', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
				if(!$this->hasErrors()) {
					$rule=new Model_Ad_Position_Rule();
					$rule->setRule($this->data->Rule);
					$rule->setAllowed($this->data->RuleAllowed);
					$this->rules[]=$rule;
				}
			} elseif(\Pecee\Integer::is_int($this->data->deleteRule)) {
				foreach($this->rules as $key=>$rule) {
					if($key==$this->data->deleteRule) {
						unset($this->rules[$key]);
					}
				}
			} else {
				$this->addInputValidation($this->_('Side'), 'SiteID', new \Pecee\UI\Form\Validate\ValidateInputInteger());
				$this->addInputValidation($this->_('Type'), 'TypeID', new \Pecee\UI\Form\Validate\ValidateInputInteger());
				$this->addInputValidation($this->_('Navn'), 'Name', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
				$this->addInputValidation($this->_('Unikt tag'), 'UniqueTag', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
				
				if(!$this->hasErrors()) {
					$position=new Model_Ad_Position();
					$position->setSiteID($this->data->SiteID);
					$position->setTypeID($this->data->TypeID);
					$position->setUniqueTag($this->data->UniqueTag);
					$position->setName($this->data->Name);
					$position->setVisible($this->data->Visible);
					$position->setDisabled(!($this->data->Active));
					$position->setRules($this->rules);
					$position->save();
					
					Model_Ad_Log::Log(sprintf('%s oprettede en ny position SiteID: %s, Unikt tag: %s, Navn: %s',
												\Pecee\Model\User\ModelUser::Current()->getUsername(),
												$this->data->SiteID,
												$this->data->UniqueTag,
												$this->data->Name));
					
					\Pecee\Router::Redirect(\Pecee\Router::GetRoute('positions', ''));
				}
			}
		}
		
	}
	
}