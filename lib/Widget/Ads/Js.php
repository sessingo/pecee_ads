<?php
class Widget_Ads_Js extends Widget_Site {
	protected $pools;
	protected $siteId;
	public function __construct($siteId=NULL) {
		$this->navActiveIndex=1;
		parent::__construct();
		$this->siteId=$siteId;
		if(!is_null($siteId)) {
			$this->pools=Model_Pool::GetBySiteId($siteId);
		}
	}
}