<?php
class Controller_Ads extends \Pecee\Controller\Controller {
	
	public static function MakeJavascriptUri($siteId, $poolId, $adId = NULL ) {
		return sprintf('<script type="text/javascript" src="http://%s/js/%s/%s/%s"></script>', 
				$_SERVER['HTTP_HOST'], $siteId, $poolId, ((!is_null($adId)) ? '?adId=' . $adId : ''));
	}
	
	public static function MakeJavascriptClickUri($adId) {
		return sprintf('http://%s/click/%s/', $_SERVER['HTTP_HOST'], $adId);
	}
	
	private function ShowMenu() {
		$menu = new \Pecee\UI\Menu\Menu();
		$menu->setClass('menu');
		if( \Pecee\Model\User\ModelUser::IsLoggedIn() && \Pecee\Model\User\ModelUser::Current()->getAdminLevel() > 1 ) {		
			$menu->addItem($this->_('Ny reklame'), \Pecee\Router::GetRoute('ads', 'new'));
			$menu->addItem($this->_('Oversigt'), \Pecee\Router::GetRoute('ads', ''));
			$menu->addItem($this->_('Generer javascript'), \Pecee\Router::GetRoute('ads', 'js'));
		} else {
			$menu->addItem($this->_('Tilføj reklame'), \Pecee\Router::GetRoute('contact', ''));
			$menu->addItem($this->_('Oversigt'), \Pecee\Router::GetRoute('ads', ''));
		}
		\Pecee\UI\Site::GetInstance()->addToLocation($menu, \Pecee\UI\Site::LOCATION_TOP);
	}
	
	public function statisticView( $AdID ) {
		$this->ShowMenu();	
		echo new Widget_Ads_Statistic($AdID);
	}
	
	public function indexView( $PageIndex = 0 ) {
		$this->ShowMenu();
		echo (\Pecee\Model\User\ModelUser::IsLoggedIn() && \Pecee\Model\User\ModelUser::Current()->getAdminLevel() > 1) ? new Widget_Ads($PageIndex) : new Widget_Ads_Partner($PageIndex);
	}
	
	public function newView($siteId=NULL) {	
		self::ShowMenu();
		echo new Widget_Ads_New($siteId);
	}
	
	public function showView( $AdID ) {
		self::ShowMenu();	
		echo new Widget_Ads_Show( $AdID );
	}
	
	public function editView($adId) {
		self::ShowMenu();	
		echo new Widget_Ads_Edit($adId);
	}
	
	public function jsView($siteId=NULL) {
		self::ShowMenu();	
		echo new Widget_Ads_Js($siteId);
	}
	
	public function deleteView( $adID ) {
		echo new Widget_Ads_Delete($adID);
	}
}