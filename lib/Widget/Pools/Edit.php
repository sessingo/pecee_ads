<?php
class Widget_Pools_Edit extends Widget_Site {
	protected $pool;
	protected $keywords;
	protected $siteId;
	public function __construct($poolId) {
		$this->navActiveIndex=3;
		parent::__construct();
		$this->pool=Model_Pool::GetById($poolId);
		if(!$this->pool->hasRow()) {
			\Pecee\Router::Redirect(\Pecee\Router::GetRoute(NULL, ''));
		}
		
		if($this->isPostBack()) {
			$this->siteId=$this->data->siteId;
			$this->isHtml=$this->data->isHtml;
			$this->keywords=\Pecee\String\Encoding::Base64Decode($this->data->keywords, array());
			if($this->data->autoPostBack != '1') {
				if($this->isPostBack()) {
					$this->addInputValidation($this->_('Navn'), 'name', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
					$this->addInputValidation($this->_('Type'), 'typeId', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
					$this->addInputValidation($this->_('Side'), 'siteId', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
					$this->addInputValidation($this->_('Aktiv fra'), 'activeFrom', new \Pecee\UI\Form\Validate\ValidateInputDate(TRUE));
					$this->addInputValidation($this->_('Aktiv til'), 'activeTo', new \Pecee\UI\Form\Validate\ValidateInputDate(TRUE));
					
					if(!$this->hasErrors()) {
						$this->pool->Name=$this->data->name;
						$this->pool->Description=$this->data->description;
						$this->pool->SiteID=$this->data->siteId;
						$this->pool->PoolTypeID=$this->data->typeId;
						$this->pool->ParentPoolID=$this->data->ParentPoolID;
						$this->pool->ActiveFrom=($this->data->activeFrom) ? \Pecee\Date::ToDateTime(strtotime($this->data->activeFrom)) : NULL;
						$this->pool->ActiveTo=($this->data->activeTo) ? \Pecee\Date::ToDateTime(strtotime($this->data->activeTo)) : NULL;
						$this->pool->UseFrame=($this->data->useFrame);
						$this->pool->Disabled=(!$this->data->disabled);
						$this->pool->RefreshRateMS=($this->data->refreshInterval) ? $this->data->refreshInterval : NULL;
						$this->pool->Takeover=$this->data->takeover;
						$this->pool->setKeywords($this->keywords);
						$this->pool->update();
						
						/* Clear cache */
						Site::Instance()->cache->clear($this->pool->getSiteID());
				
						\Pecee\Router::Redirect(\Pecee\Router::GetRoute(NULL, ''));
					}
				}
			} else {
				if($this->data->addKeyword && $this->data->keyword) {
					$this->keywords[]=$this->data->keyword;
				} elseif(\Pecee\Integer::is_int($this->data->keywordToDelete)) {
					unset($this->keywords[$this->data->keywordToDelete]);
				}
			}
		} else {
			$this->siteId=$this->pool->getSiteID();
			$keywords=$this->pool->getKeywords();
			if($keywords) {
				foreach($keywords as $word) {
					$this->keywords[]=$word->getKeyword();
				}
			}
		}	
	}
}