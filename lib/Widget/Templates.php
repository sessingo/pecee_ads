<?php
class Widget_Templates extends Widget_Site {
	protected $page;
	protected $templates;
	
	public function __construct() {
		$this->navActiveIndex=2;
		parent::__construct();
		$this->page=$this->getParam('page',0);
		$query = $this->getParam('query');
		$this->templates = Model_Ad_Template::Get($query, NULL, 25, $this->page);
	}
	
}