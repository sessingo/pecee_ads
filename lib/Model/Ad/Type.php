<?php
class Model_Ad_Type extends \Pecee\Model\Model {
	public function __construct() {
		parent::__construct('AdType', array('AdTypeID' => NULL, 'Width' => NULL, 'Height' => NULL));
	}
	
	public function save() {
		$this->AdTypeID=parent::save()->getInsertId();
	}
	
	/**
	 * Get type by id
	 * @param string $TypeId
	 * @return Model_Ad_Type
	 */
	public static function GetById($typeId) {
		return self::FetchOne('SELECT * FROM `AdType` WHERE `AdTypeID` = %s', $typeId);
	}
	
	/**
	 * Get all ads or filter by width, height or both.
	 * @param int|null $width
	 * @param int|null $height
	 * @param int|null $rows
	 * @param int|null $page
	 * @return Model_Ad_Type
	 */
	public static function Get($width=NULL, $height=NULL, $rows=25, $page=0) {
		$where = array('1=1');
		if(!is_null($width) || !is_null($width)) {
			$where[] = \Pecee\DB\DB::FormatQuery('`Width` LIKE %s OR `Height` LIKE %s', array($width, $height));
		}
		return self::FetchPage('`AdTypeID`', 'SELECT * FROM `AdType` WHERE ' . join(' AND ', $where), $rows, $page);
	}
}