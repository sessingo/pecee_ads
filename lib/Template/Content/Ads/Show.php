<?php /* @var $this Widget_Ads_Show */ ?>
<h3><?= $this->_('Vis reklame')?>: <?= $this->ad->Name ?></h3>
<div style="position:relative;">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP) ?>
	<div style="position:absolute;top:0px;right:0px;">
		<a href="<?= \Pecee\Router::GoBack(true); ?>"><?= $this->_('Gå tilbage')?></a>
	</div>
</div>
<table cellspacing="0" cellpadding="0" width="100%" class="margin-top padding-top">
	<tr>
		<td width="20%">
			<?= $this->_('Klik tracker')?>
		</td>
		<td>
			<?= $this->form()->input('clickTrakcer', 'text', Controller_Ads::MakeJavascriptClickUri($this->ad->AdID))
				->addAttribute('style', 'width:400px;')
				->addAttribute('onclick', 'this.select();') ?>
		</td>
	</tr>
</table>