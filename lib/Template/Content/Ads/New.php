<?php /* @var $this Widget_Ads_New */ ?>
<h3><?= $this->_('Ny reklame')?></h3>
<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP) ?>
<?= $this->form()->start('newAd', 'post')?>
<?= $this->form()->input('pools', 'hidden', \Pecee\String\Encoding::Base64Encode($this->pools)); ?>
<?= $this->form()->input('autoPostBack', 'hidden', '0')->addAttribute('ID', 'autoPostBack') ?>
<?= $this->form()->input('poolToDelete', 'hidden', '')->addAttribute('ID', 'poolToDelete'); ?>
<?= $this->showErrors('newAd'); ?>
<table cellspacing="0" cellpadding="0" class="margin-top" width="100%">
	<tr>
		<td width="30%">
			<?= $this->_('Navn') ?>
		</td>
		<td>
			<?= $this->form()->input('Name', 'text', NULL, TRUE); ?>
		</td>
	</tr>
	<tr>
		<td class="padding-top" style="border-bottom:1px solid #EEE;padding-bottom:5px;">
			<h4 style="margin:0;padding:0;font-size:16px;"><?= $this->_('Tilføj pool')?></h4>
		</td>
		<td class="padding-top" style="border-bottom:1px solid #EEE;padding-bottom:5px;">
			<h4 style="margin:0;padding:0;font-size:16px;"><?= $this->_('Eksisterende pools')?></h4>
		</td>
	</tr>
	<tr>
		<td class="padding-top" valign="top">
			<?= $this->form()->selectStart('siteId', new Dataset_Sites($this->_('-- Vælg site --')))->addAttribute('onchange', 'document.getElementById(\'autoPostBack\').value = \'1\';form.submit();'); ?> 
			<?= $this->form()->selectStart('poolId', new Dataset_Pools($this->_('-- Vælg pool --'), $this->data->siteId, $this->poolIdsToExclude))->addAttribute(((!$this->data->siteId) ? 'disabled' : NULL), ((!$this->data->siteId) ? 'disabled' : NULL)); ?> 
			<?= $this->form()->submit('addPool', $this->_('Tilføj'))->addAttribute('onclick', 'document.getElementById(\'autoPostBack\').value = \'1\';')?>		
		</td>
		<td valign="top" style="padding-bottom:20px;padding-top:8px;">
			<? if($this->pools && count($this->pools) > 0) : ?>				
				<table style="width:100%;">
					<? foreach($this->pools as $key=>$pool) : ?>
					<tr>
						<td style="width:30%;padding-bottom:8px;">
							<?= $pool->getName(); ?>
						</td>
						<td style="text-align:right;padding-left:20px;padding-bottom:8px;">
							<a href="javascript:;" onclick="document.getElementById('autoPostBack').value = '1';document.getElementById('poolToDelete').value = '<?= $key; ?>';document.newAd.submit();"><?= $this->_('Slet'); ?></a>
						</td>
					</tr>
					<? endforeach;?>
				</table>
			<? else: ?>
				<span style="color:#999;"><?= $this->_('Ingen pools tilføjet')?></span>
			<? endif; ?>
		</td>
	</tr>
	<tr>
		<td class="padding-top">
			<?= $this->_('Indhold er html')?>
		</td>
		<td class="padding-top">
			<?= $this->form()->bool('isHtml', $this->isHtml, TRUE)->addAttribute('onclick', 'document.getElementById(\'autoPostBack\').value = \'1\';form.submit();') ?>
		</td>
	</tr>
	<? if(!$this->isHtml) : ?>
	<tr>
		<td width="30%" class="padding-top">
			<?= $this->_('Klik url') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->input('url', 'text'); ?>
		</td>
	</tr>
	<? endif; ?>
	<tr>
		<td width="30%" class="padding-top" valign="top">
			<?= ($this->isHtml) ? $this->_('Indsæt HTML') : $this->_('Indholds url') ?>
		</td>
		<td class="padding-top">
			<?= ($this->isHtml) ? $this->form()->textarea('content', 10, 50) : $this->form()->input('Content', 'text'); ?>
		</td>
	</tr>
	<tr>
		<td width="30%" class="padding-top">
			<?= $this->_('Visnings procent') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->input('showPercentage', 'text', '10', true)->addAttribute('maxlength', '3'); ?>
		</td>
	</tr>
	<tr>
		<td width="30%" class="padding-top">
			<?= $this->_('Udløbs dato') ?> (dd-mm-yyyy)
		</td>
		<td class="padding-top">
			<?= $this->form()->input('expiresDate', 'text'); ?>
		</td>
	</tr>
	<tr>
		<td width="30%" class="padding-top">
			<?= $this->_('Max visninger') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->input('maxViews', 'text'); ?>
		</td>
	</tr>
	<tr>
		<td width="30%" class="padding-top">
			<?= $this->_('Aktiv') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->input('active', 'checkbox', TRUE)->addAttribute('checked', 'true'); ?>
		</td>
	</tr>
	<tr>
		<td width="30%">
		</td>
		<td class="padding-top">
			<?= $this->form()->submit('submit', $this->_('Gem'))->addAttribute('class', 'margin-top'); ?>
		</td>
	</tr>
</table>
<?= $this->form()->end();?>