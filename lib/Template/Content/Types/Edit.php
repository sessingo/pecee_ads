<?php /* @var $this \Pecee\UI\Site */ ?>
<h3><?= $this->_('Rediger type')?></h3>
<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP) ?>
<?= $this->form()->start('newSite')?>
<?= $this->showErrors(); ?>

<table cellspacing="0" cellpadding="0" width="100%" class="margin-top">
	<tr>
		<td width="30%">
			<?= $this->_('Bredde') ?>
		</td>
		<td>
			<?= $this->form()->input('Width', 'text', $this->type->Width)->addAttribute('maxlength', '3')?>
		</td>
	</tr>
	<tr>
	<tr>
		<td width="30%" class="padding-top">
			<?= $this->_('Højde') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->input('Height', 'text', $this->type->Height)->addAttribute('maxlength', '3'); ?>
		</td>
	</tr>
	<tr>
		<td width="30%">
		</td>
		<td class="padding-top">
			<?= $this->form()->submit('submit', $this->_('Gem'))->addAttribute('class', 'margin-top'); ?>
		</td>
	</tr>
</table>
<?= $this->form()->end();?>