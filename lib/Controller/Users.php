<?php
class Controller_Users extends \Pecee\Controller\Controller {
	
	private function ShowMenu() {
		$menu = new \Pecee\UI\Menu\Menu();
		$menu->setClass('menu');
		$menu->addItem($this->_('Ny bruger'), \Pecee\Router::GetRoute('users', 'new'));
		$menu->addItem($this->_('Oversigt'), \Pecee\Router::GetRoute('users', ''));
		\Pecee\UI\Site::GetInstance()->addToLocation($menu, \Pecee\UI\Site::LOCATION_TOP);
	}
	
	public function indexView( $PageIndex = 0 ) {
		$this->ShowMenu();
		$users = new Widget_Users( $PageIndex );
		echo $users;
	}
	
	public function newView() {
		$this->ShowMenu();
		$widget = new Widget_Users_New();
		echo $widget;
	}
	
	public function deleteView( $UserID ) {
		$this->ShowMenu();
		$widget = new Widget_Users_Delete($UserID);
		echo $widget;
	}
	
	public function editView( $UserID ) {
		$this->ShowMenu();
		$widget = new Widget_Users_Edit( $UserID );
		echo $widget;
	}
	
}