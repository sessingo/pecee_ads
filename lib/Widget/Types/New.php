<?php
class Widget_Types_New extends Widget_Site {
	public function __construct() {
		$this->navActiveIndex=4;
		parent::__construct();
		$this->addInputValidation($this->_('Højde'), 'Height', new \Pecee\UI\Form\Validate\ValidateInputInteger());
		$this->addInputValidation($this->_('Bredde'), 'Width', new \Pecee\UI\Form\Validate\ValidateInputInteger());
		if($this->isPostBack() && !$this->hasErrors()) {
			$type=new Model_Pool_Type();
			$type->setWidth($this->data->Width);
			$type->setHeight($this->data->Height);
			$type->save();
			
			Model_Ad_Log::Log(sprintf('%s oprettede en ny type %sx%s (%s)', 
									\Pecee\Model\User\ModelUser::Current()->getUsername(),
									$type->getWidth(),
									$type->getHeight(),
									$type->getPoolTypeID()));
			
			\Pecee\Router::Redirect( \Pecee\Router::GetRoute('types', '') );
		}
	}
}