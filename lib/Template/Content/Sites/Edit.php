<?php /* @var $this Widget_Sites_New */ ?>
<h3><?= $this->_('Rediger side')?></h3>
<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP) ?>
<?= $this->form()->start('newSite')?>
<?= $this->showErrors(); ?>

<?= $this->data->Name; ?>
<table cellspacing="0" cellpadding="0" width="100%" class="margin-top">
	<tr>
		<td width="30%">
			<?= $this->_('Navn') ?>
		</td>
		<td>
			<?= $this->form()->input('Name', 'text', $this->site->getName()); ?>
		</td>
	</tr>
	<tr>
		<td width="30%" class="padding-top">
			<?= $this->_('URL') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->input('Url', 'text', $this->site->getUrl()); ?>
		</td>
	</tr>
	<tr>
		<td width="30%" class="padding-top">
			<?= $this->_('Synlig') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->bool('Visible', $this->site->getVisible()); ?>
		</td>
	</tr>
	<tr>
		<td width="30%" class="padding-top">
			<?= $this->_('Aktiv') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->bool('Disabled', !$this->site->getDisabled()); ?>
		</td>
	</tr>
	<tr>
		<td width="30%">
		</td>
		<td class="padding-top">
			<?= $this->form()->submit('submit', $this->_('Gem'))->addAttribute('class', 'margin-top'); ?>
		</td>
	</tr>
</table>
<?= $this->form()->end();?>