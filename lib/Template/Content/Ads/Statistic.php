<? /* @var $this Widget_Ads_Statistic */ ?>
<h3><?= $this->_('Statistik for')?>: <?= $this->ad->Name; ?></h3>
<div style="position:relative;">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP) ?>
	<div style="position:absolute;top:0px;right:0px;">
		<a href="<?= \Pecee\Router::GoBack(true); ?>"><?= $this->_('Gå tilbage')?></a>
	</div>
</div>
<? if($this->ad->getClicks() == 0 && !$this->ad->IsHtml) : ?>
<div class="error" style="text-align:left;font-weight:normal;">
	<b><?= $this->_('VIGTIGT')?>:</b><br/>
	<?= $this->_('Det ser ikke ud til, at du benytter vores klik-tracker til at registrere kliks')?>.<br/>
	<?= $this->_('Statistikken kan derfor være ukorrekt.')?><br/>
	<?= $this->_('For at benytte vores klik-tracker indsæt venligst følgende URL i dine bannere')?>:<br/><br/>
	<code>
		<?= Controller_Ads::MakeJavascriptClickUri($this->ad->AdID) ?>
	</code>
</div>
<? endif; ?>

<table cellspacing="0" cellpadding="0" width="100%" class="margin-top padding-bottom">
	<tr>
		<td width="30%">
			<b><?= $this->_('Navn')?></b>
		</td>
		<td>
			<?= $this->ad->Name; ?>
		</td>
	</tr>
	<tr>
		<td class="padding-top">
			<b><?= $this->_('Oprettet')?></b>
		</td>
		<td class="padding-top">
			<?= date('d/m-Y H:i:s', strtotime($this->ad->getCreatedDate())); ?>
		</td>
	</tr>
	<tr>
		<td class="padding-top">
			<b><?= $this->_('Udløber')?></b>
		</td>
		<td class="padding-top">
			<?= ($this->ad->ExpiresDate) ? date('d/m-Y H:i:s', strtotime($this->ad->getExpiresDate())) : $this->_('Aldrig'); ?>
		</td>
	</tr>
	<tr>
		<td class="padding-top">
			<b><?= $this->_('Visninger')?></b>
		</td>
		<td class="padding-top">
			<?= $this->ad->Views; ?> <?= ($this->ad->MaxViews) ? ' / ' . $this->ad->MaxViews : '' ?>
		</td>
	</tr>
	<tr>
		<td class="padding-top">
			<b><?= $this->_('Unikke visninger')?></b>
		</td>
		<td class="padding-top">
			<?= $this->ad->UniqueViews; ?>
		</td>
	</tr>
	<tr>
		<td class="padding-top">
			<b><?= $this->_('Kliks')?></b>
		</td>
		<td class="padding-top">
			<?= $this->ad->Clicks; ?>
		</td>
	</tr>
	<tr>
		<td class="padding-top">
			<b><?= $this->_('Unikke kliks')?></b>
		</td>
		<td class="padding-top">
			<?= $this->ad->UniqueClicks; ?>
		</td>
	</tr>
	<?php /*<tr>
		<td class="padding-top">
			<b><?= $this->_('Visninger (seneste 24-timer)')?></b>
		</td>
		<td class="padding-top">
			<?= $this->statistic->UniqueViews; ?>
		</td>
	</tr>
	<tr>
		<td class="padding-top padding-bottom">
			<b><?= $this->_('Kliks (seneste 24-timer)')?></b>
		</td>
		<td class="padding-top padding-bottom">
			<?= $this->statistic->UniqueClicks; ?>
		</td>
	</tr>*/ ?>
</table>
 <?php  /*
<h3 class="margin-top"><?= $this->_('7-dages diagram for visninger')?></h3>
<table cellspacing="0" cellpadding="0" width="100%" class="padding-bottom">
	<tr>
		<td width="18%" style="border-right:1px solid #CCC;">
			<b><?= $this->_('I dag')?></b>
		</td>
		<td width="3%">
		</td>
		<td class="bar">
			<div class="total">
				<span>
					<?= $this->weekStatistic->Day1Views ?> 
					<?= $this->_('unikke visninger')?> - 
					<?= ($this->ad->UniqueViews != 0) ? round( $this->weekStatistic->Day1Views / $this->ad->UniqueViews * 100) : 0 ?>%
				</span>
				<div style="width:<?= (($this->ad->UniqueViews != 0) ? round( $this->weekStatistic->Day1Views / $this->ad->UniqueViews * 637) : 0) ?>px;">
					&nbsp;
				</div>
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="3" width="100%" height="10%">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td width="15%" style="border-right:1px solid #CCC;">
			<b><?= $this->_('I går')?></b>
		</td>
		<td width="3%">
		</td>
		<td class="bar">
			<div class="total">
				<span>
					<?= $this->weekStatistic->Day2Views ?> 
					<?= $this->_('unikke visninger')?> - 
					<?= ($this->ad->UniqueViews != 0) ? round( $this->weekStatistic->Day2Views / $this->ad->UniqueViews * 100) : 0 ?>% 
				</span>
				<div style="width:<?= (($this->ad->UniqueViews != 0) ? round( $this->weekStatistic->Day2Views / $this->ad->UniqueViews * 637) : 0) ?>px;">
					&nbsp;
				</div>
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="3" width="100%" height="10%">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td width="15%" style="border-right:1px solid #CCC;">
			<b><?= date( 'd/m - Y', time() - (2*24*60*60) ) ?></b>
		</td>
		<td width="3%">
		</td>
		<td class="bar">
			<div class="total">
				<span>
					<?= $this->weekStatistic->Day3Views ?> 
					<?= $this->_('unikke visninger')?> - 
					<?= ($this->ad->UniqueViews != 0) ? round( $this->weekStatistic->Day3Views / $this->ad->UniqueViews * 100) : 0 ?>% 
				</span>
				<div style="width:<?= (($this->ad->UniqueViews != 0) ? round( $this->weekStatistic->Day3Views / $this->ad->UniqueViews * 637) : 0) ?>px;">
					&nbsp;
				</div>
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="3" width="100%" height="10%">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td width="15%" style="border-right:1px solid #CCC;">
			<b><?= date( 'd/m - Y', time() - (3*24*60*60) ) ?></b>
		</td>
		<td width="3%">
		</td>
		<td class="bar">
			<div class="total">
				<span>
					<?= $this->weekStatistic->Day4Views ?> 
					<?= $this->_('unikke visninger')?> - 
					<?= ($this->ad->UniqueViews != 0) ? round( $this->weekStatistic->Day4Views / $this->ad->UniqueViews * 100) : 0 ?>% 
				</span>
				<div style="width:<?= (($this->ad->UniqueViews != 0) ? round( $this->weekStatistic->Day4Views / $this->ad->UniqueViews * 637) : 0) ?>px;">
					&nbsp;
				</div>
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="3" width="100%" height="10%">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td width="15%" style="border-right:1px solid #CCC;">
			<b><?= date( 'd/m - Y', time() - (4*24*60*60) ) ?></b>
		</td>
		<td width="3%">
		</td>
		<td class="bar">
			<div class="total">
				<span>
					<?= $this->weekStatistic->Day5Views ?> 
					<?= $this->_('unikke visninger')?> - 
					<?= ($this->ad->UniqueViews != 0) ? round( $this->weekStatistic->Day5Views / $this->ad->UniqueViews * 100) : 0 ?>% 
				</span>
				<div style="width:<?= (($this->ad->UniqueViews != 0) ? round( $this->weekStatistic->Day5Views / $this->ad->UniqueViews * 637) : 0) ?>px;">
					&nbsp;
				</div>
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="3" width="100%" height="10%">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td width="15%" style="border-right:1px solid #CCC;">
			<b><?= date( 'd/m - Y', time() - (5*24*60*60) ) ?></b>
		</td>
		<td width="3%">
		</td>
		<td class="bar">
			<div class="total">
				<span>
					<?= $this->weekStatistic->Day6Views ?> 
					<?= $this->_('unikke visninger')?> - 
					<?= ($this->ad->UniqueViews != 0) ? round( $this->weekStatistic->Day6Views / $this->ad->UniqueViews * 100) : 0 ?>% 
				</span>
				<div style="width:<?= (($this->ad->UniqueViews != 0) ? round( $this->weekStatistic->Day6Views / $this->ad->UniqueViews * 637) : 0) ?>px;">
					&nbsp;
				</div>
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="3" width="100%" height="10%">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td width="15%" style="border-right:1px solid #CCC;">
			<b><?= date( 'd/m - Y', time() - (6*24*60*60) ) ?></b>
		</td>
		<td width="3%">
		</td>
		<td class="bar">
			<div class="total">
				<span>
					<?= $this->weekStatistic->Day7Views ?> 
					<?= $this->_('unikke visninger')?> - 
					<?= ($this->ad->UniqueViews != 0) ? round( $this->weekStatistic->Day7Views / $this->ad->UniqueViews * 100) : 0 ?>% 
				</span>
				<div style="width:<?= (($this->ad->UniqueViews != 0) ? round( $this->weekStatistic->Day7Views / $this->ad->UniqueViews * 637) : 0) ?>px;">
					&nbsp;
				</div>
			</div>
		</td>
	</tr>
</table>

<h3 class="margin-top"><?= $this->_('7-dages diagram for kliks')?></h3>
<table cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td width="18%" style="border-right:1px solid #CCC;">
			<b><?= $this->_('I dag')?></b>
		</td>
		<td width="3%">
		</td>
		<td class="bar">
			<div class="total">
				<span>
					<?= $this->weekStatistic->Day1clicks ?> 
					<?= $this->_('unikke kliks')?> - 
					<?= ($this->ad->Uniqueclicks != 0) ? round( $this->weekStatistic->Day1clicks / $this->ad->Uniqueclicks * 100) : 0 ?>%
				</span>
				<div style="width:<?= (($this->ad->Uniqueclicks != 0) ? round( $this->weekStatistic->Day1clicks / $this->ad->Uniqueclicks * 637) : 0) ?>px;">
					&nbsp;
				</div>
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="3" width="100%" height="10%">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td width="15%" style="border-right:1px solid #CCC;">
			<b><?= $this->_('I går')?></b>
		</td>
		<td width="3%">
		</td>
		<td class="bar">
			<div class="total">
				<span>
					<?= $this->weekStatistic->Day2clicks ?> 
					<?= $this->_('unikke kliks')?> - 
					<?= ($this->ad->Uniqueclicks != 0) ? round( $this->weekStatistic->Day2clicks / $this->ad->Uniqueclicks * 100) : 0 ?>% 
				</span>
				<div style="width:<?= (($this->ad->Uniqueclicks != 0) ? round( $this->weekStatistic->Day2clicks / $this->ad->Uniqueclicks * 637) : 0) ?>px;">
					&nbsp;
				</div>
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="3" width="100%" height="10%">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td width="15%" style="border-right:1px solid #CCC;">
			<b><?= date( 'd/m - Y', time() - (2*24*60*60) ) ?></b>
		</td>
		<td width="3%">
		</td>
		<td class="bar">
			<div class="total">
				<span>
					<?= $this->weekStatistic->Day3clicks ?> 
					<?= $this->_('unikke kliks')?> - 
					<?= ($this->ad->Uniqueclicks != 0) ? round( $this->weekStatistic->Day3clicks / $this->ad->Uniqueclicks * 100) : 0 ?>% 
				</span>
				<div style="width:<?= (($this->ad->Uniqueclicks != 0) ? round( $this->weekStatistic->Day3clicks / $this->ad->Uniqueclicks * 637) : 0) ?>px;">
					&nbsp;
				</div>
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="3" width="100%" height="10%">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td width="15%" style="border-right:1px solid #CCC;">
			<b><?= date( 'd/m - Y', time() - (3*24*60*60) ) ?></b>
		</td>
		<td width="3%">
		</td>
		<td class="bar">
			<div class="total">
				<span>
					<?= $this->weekStatistic->Day4clicks ?> 
					<?= $this->_('unikke kliks')?> - 
					<?= ($this->ad->Uniqueclicks != 0) ? round( $this->weekStatistic->Day4clicks / $this->ad->Uniqueclicks * 100) : 0 ?>% 
				</span>
				<div style="width:<?= (($this->ad->Uniqueclicks != 0) ? round( $this->weekStatistic->Day4clicks / $this->ad->Uniqueclicks * 637) : 0) ?>px;">
					&nbsp;
				</div>
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="3" width="100%" height="10%">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td width="15%" style="border-right:1px solid #CCC;">
			<b><?= date( 'd/m - Y', time() - (4*24*60*60) ) ?></b>
		</td>
		<td width="3%">
		</td>
		<td class="bar">
			<div class="total">
				<span>
					<?= $this->weekStatistic->Day5clicks ?> 
					<?= $this->_('unikke kliks')?> - 
					<?= ($this->ad->Uniqueclicks != 0) ? round( $this->weekStatistic->Day5clicks / $this->ad->Uniqueclicks * 100) : 0 ?>% 
				</span>
				<div style="width:<?= (($this->ad->Uniqueclicks != 0) ? round( $this->weekStatistic->Day5clicks / $this->ad->Uniqueclicks * 637) : 0) ?>px;">
					&nbsp;
				</div>
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="3" width="100%" height="10%">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td width="15%" style="border-right:1px solid #CCC;">
			<b><?= date( 'd/m - Y', time() - (5*24*60*60) ) ?></b>
		</td>
		<td width="3%">
		</td>
		<td class="bar">
			<div class="total">
				<span>
					<?= $this->weekStatistic->Day6clicks ?> 
					<?= $this->_('unikke kliks')?> - 
					<?= ($this->ad->Uniqueclicks != 0) ? round( $this->weekStatistic->Day6clicks / $this->ad->Uniqueclicks * 100) : 0 ?>% 
				</span>
				<div style="width:<?= (($this->ad->Uniqueclicks != 0) ? round( $this->weekStatistic->Day6clicks / $this->ad->Uniqueclicks * 637) : 0) ?>px;">
					&nbsp;
				</div>
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="3" width="100%" height="10%">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td width="15%" style="border-right:1px solid #CCC;">
			<b><?= date( 'd/m - Y', time() - (6*24*60*60) ) ?></b>
		</td>
		<td width="3%">
		</td>
		<td class="bar">
			<div class="total">
				<span>
					<?= $this->weekStatistic->Day7clicks ?> 
					<?= $this->_('unikke kliks')?> - 
					<?= ($this->ad->Uniqueclicks != 0) ? round( $this->weekStatistic->Day7clicks / $this->ad->Uniqueclicks * 100) : 0 ?>% 
				</span>
				<div style="width:<?= (($this->ad->Uniqueclicks != 0) ? round( $this->weekStatistic->Day7clicks / $this->ad->Uniqueclicks * 637) : 0) ?>px;">
					&nbsp;
				</div>
			</div>
		</td>
	</tr>
</table>

*/ ?>
