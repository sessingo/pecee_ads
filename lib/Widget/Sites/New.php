<?php
class Widget_Sites_New extends Widget_Site {
	public function __construct() {
		$this->navActiveIndex=5;
		parent::__construct();
		$this->addInputValidation($this->_('Navn'), 'Name', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
		$this->addInputValidation($this->_('Url'), 'Url', new \Pecee\UI\Form\Validate\ValidateInputUri());
	
		if($this->isPostBack() && !$this->hasErrors()) {
			$site=new Model_Ad_Site();
			$site->setName($this->data->Name);
			$site->setUrl($this->data->Url);
			$site->setVisible(($this->data->Visible));
			$site->setDisabled(!($this->data->Disabled));
			$site->save();
			
			Model_Ad_Log::Log(sprintf('%s oprettede et nyt site: %s (%s)', \Pecee\Model\User\ModelUser::Current()->getUsername(), $this->data->Name, $this->data->Url));
			
			\Pecee\Router::Redirect(\Pecee\Router::GetRoute('sites', ''));
		}
	}
}