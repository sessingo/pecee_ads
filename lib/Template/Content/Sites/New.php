<?php /* @var $this Widget_Sites_New */ ?>
<h3><?= $this->_('Ny side')?></h3>
<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP) ?>
<?= $this->form()->start('newSite')?>
<?= $this->showErrors(); ?>

<table cellspacing="0" cellpadding="0" width="100%" class="margin-top">
	<tr>
		<td width="30%">
			<?= $this->_('Navn') ?>
		</td>
		<td>
			<?= $this->form()->input('Name', 'text'); ?>
		</td>
	</tr>
	<tr>
		<td width="30%" class="padding-top">
			<?= $this->_('URL') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->input('Url', 'text'); ?>
		</td>
	</tr>
	<tr>
		<td width="30%" class="padding-top">
			<?= $this->_('Synlig') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->bool('Visible'); ?>
		</td>
	</tr>
	<tr>
		<td width="30%" class="padding-top">
			<?= $this->_('Aktiv') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->bool('Disabled'); ?>
		</td>
	</tr>
	<tr>
		<td width="30%">
		</td>
		<td class="padding-top">
			<?= $this->form()->submit('submit', $this->_('Gem'))->addAttribute('class', 'margin-top'); ?>
		</td>
	</tr>
</table>
<?= $this->form()->end();?>