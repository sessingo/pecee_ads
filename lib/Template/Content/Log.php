<? /* @var $this Widget_Log */ ?>
<h3><?= $this->_('Log')?></h3>
<?= $this->form()->start( 'LogStart' ) ?>
<table cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td class="padding-bottom">
			<?= $this->_('Søgeord') ?>: <?= $this->form()->input('query', 'text') ?>&nbsp;&nbsp;
			<?= $this->_('Beskeder per linje')?>: <?= $this->form()->input('rows', 'text')->addAttribute('maxlength', '3') ?>&nbsp;&nbsp;
			<?= $this->form()->submit('Update', $this->_('Update'))?>
			<div class="hr margin-top"></div>
		</td>
	</tr>
	<tr>
		<td class="padding-top">
			<? if($this->logs->hasRows()) : ?>
			<table cellspacing="0" width="100%" cellpadding="0">
				<tr>
					<td width="20%" class="bold padding-bottom">
						<?= $this->_('Tidspunkt')?>
					</td>
					<td class="bold padding-bottom">
						<?= $this->_('Meddelelse')?>
					</td>
				</tr>	
			<? foreach($this->logs->getRows() as $log) : ?>
				<tr> 
					<td height="25">
						<?= date('d/m-Y H:i:s', $log->getDate())?>
					</td>
					<td>
						<?= $log->getDescription(); ?>
					</td>
				</tr>
				<? endforeach; ?>
				<tr>
					<td colspan="7" width="100%" align="center" class="padding-top">
						<div class="padding-top margin-top large">
							<?= \Pecee\UI\Paging::DisplayNumberedPaging(6, $this->page, $this->logs->getMaxPages(), \Pecee\Router::GetRoute('log', '', null, array('page' => '%1$d')), $this->_('Forrige'), $this->_('Næste'))?>
						</div>
					</td>
				</tr>
			</table>
			<? endif;?>
		</td>
	</tr>
</table>
<?= $this->form()->end(); ?>