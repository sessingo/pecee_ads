<?php
class Dataset_Sites extends \Pecee\Dataset {
	
	public function __construct($choose=NULL, $visible = NULL) {
		if(!is_null($choose)) {
			$this->createArray('', $choose);
		}
		$sites = Model_Ad_Site::Get(NULL, $visible, NULL, NULL);
		if($sites->hasRows()) {
			foreach($sites->getRows() as $site) {
				$this->createArray($site->SiteID, $site->Name);
			}
		}
	}
	
}