<? /* @var $this Widget_Pools_New */ ?>
<h3><?= $this->_('Ny pool')?></h3>
<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP) ?>
<?= $this->form()->start('createPool')?>
<?= $this->form()->input('autoPostBack', 'hidden', '0')->addAttribute('ID', 'autoPostBack') ?>
<?= $this->form()->input('keywords', 'hidden', \Pecee\String\Encoding::Base64Encode($this->keywords)); ?>
<?= $this->form()->input('keywordToDelete', 'hidden', '')->addAttribute('ID', 'keywordToDelete'); ?>
<?= $this->showErrors(); ?>
<table cellspacing="0" cellpadding="0" width="100%" class="margin-top">
	<tr>
		<td width="30%">
			<?= $this->_('Navn') ?>
		</td>
		<td>
			<?= $this->form()->input('name', 'text'); ?>
		</td>
	</tr>
	<tr>
		<td class="padding-top" valign="top">
			<?= $this->_('Beskrivelse') ?>
		</td>
		<td class="padding-top" valign="top">
			<?= $this->form()->textarea('description', 10, 50)?>
		</td>
	</tr>
	<tr>
		<td class="padding-top" style="border-bottom:1px solid #EEE;padding-bottom:5px;">
			<h4 style="margin:0;padding:0;font-size:16px;"><?= $this->_('Tilføj keyword')?></h4>
		</td>
		<td class="padding-top" style="border-bottom:1px solid #EEE;padding-bottom:5px;">
			<h4 style="margin:0;padding:0;font-size:16px;"><?= $this->_('Eksisterende keywords')?></h4>
		</td>
	</tr>
	<tr>
		<td class="padding-top" valign="top">
			<?= $this->form()->input('keyword', 'text', ''); ?> 
			<?= $this->form()->submit('addKeyword', $this->_('Tilføj'))->addAttribute('onclick', 'document.getElementById(\'autoPostBack\').value = \'1\';')?>		
		</td>
		<td valign="top" style="padding-bottom:20px;padding-top:8px;">
			<? if($this->keywords && count($this->keywords) > 0) : ?>				
				<table style="width:100%;">
					<? foreach($this->keywords as $key=>$keyword) : ?>
					<tr>
						<td style="width:30%;padding-bottom:8px;">
							<?= $keyword; ?>
						</td>
						<td style="text-align:right;padding-left:20px;padding-bottom:8px;">
							<a href="javascript:;" onclick="document.getElementById('autoPostBack').value = '1';document.getElementById('keywordToDelete').value = '<?= $key; ?>';document.createPool.submit();"><?= $this->_('Slet'); ?></a>
						</td>
					</tr>
					<? endforeach;?>
				</table>
			<? else: ?>
				<span style="color:#999;"><?= $this->_('Ingen keywords tilføjet')?></span>
			<? endif; ?>
		</td>
	</tr>
	<tr>
		<td class="padding-top">
			<?= $this->_('Type') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->selectStart('typeId', new Dataset_Types($this->_('-- Vælg --')))?>
		</td>
	</tr>
	<tr>
		<td class="padding-top">
			<?= $this->_('Side') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->selectStart('siteId', new Dataset_Sites($this->_('-- Vælg --')))->addAttribute('onchange', 'document.getElementById(\'autoPostBack\').value=\'1\';form.submit();')?>
		</td>
	</tr>
	<tr>
		<td class="padding-top">
			<?= $this->_('Nedarv fra anden pool') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->selectStart('parentPoolId', new Dataset_Pools($this->_('Ingen'), $this->data->siteId))
				->addAttribute(((!$this->data->siteId) ? 'disabled' : ''), (($this->data->siteId) ? 'disabled' : '')); ?>
		</td>
	</tr>
	<tr>
		<td class="padding-top">
			<?= $this->_('Aktiv fra') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->input('activeFrom', 'text', NULL, TRUE) ?>
		</td>
	</tr>
	<tr>
		<td class="padding-top">
			<?= $this->_('Aktiv til') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->input('activeTo', 'text', NULL, TRUE) ?>
		</td>
	</tr>
	<tr>
		<td class="padding-top">
			<?= $this->_('Refresh interval (MS)') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->input('refreshInterval', 'text') ?>
		</td>
	</tr>
	<tr>
		<td class="padding-top">
			<?= $this->_('Takeover other pools') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->bool('takeover', FALSE) ?>
		</td>
	</tr>
	<tr>
		<td class="padding-top">
			<?= $this->_('Benyt iframe') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->bool('useFrame') ?>
		</td>
	</tr>
	<tr>
		<td class="padding-top">
			<?= $this->_('Aktiv') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->bool('disabled', TRUE) ?>
		</td>
	</tr>
	<tr>
		<td></td>
		<td class="padding-top">
			<?= $this->form()->submit('submit', $this->_('Gem'))->addAttribute('class', 'margin-top'); ?>
		</td>
	</tr>
</table>
<?= $this->form()->end();?>