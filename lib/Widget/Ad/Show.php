<?php
class Widget_Ad_Show extends \Pecee\Widget\Widget {
	protected $pool;
	protected $poolIds;
	protected $siteId;
	protected $type;
	protected $keywords;
	protected $output;
	public function __construct() {
		parent::__construct();
		$this->setTemplate(NULL);
				
		if(!$this->getParam('pools', FALSE) || !$this->getParam('site',FALSE)) {
			die();
		}
		if($this->hasParam('keywords')) {
			$this->keywords=explode(',',$this->getParam('keywords'));
		}
		$poolIds=explode(',', $this->getParam('pools'));
		$this->siteId=$this->getParam('site');
		$pools=Model_Pool::GetAdPool($this->siteId, $poolIds, $this->keywords);
		if(!$pools || $pools instanceof Model_Pool && !$pools->hasRows()) {
			die();
		}
		
		$this->poolIds=array();
		$this->pool=$pools->getRow(0);
		
		/* @var $pool Model_Pool */
		foreach($pools->getRows() as $i=>$pool) {
			$this->poolIds[]=$pool->getPoolID();
			if($pool->getTakeOver()) {
				$this->poolIds=array($pool->getPoolID());
				$this->pool=$pool;
				break;
			}
		}
		
		$this->type=$this->pool->getType();
		if(!$this->type->hasRow()) {
			die();
		}
		$content='';
		if($this->pool->getUseFrame()) {
			$content=$this->writeFrame();
		} else {
			$content = new Widget_Ad_Single($this->siteId, $this->poolIds);
		}

		/* Make <script> tags save */
		$content=str_replace('\'', '\\\'', $content);
		$content=str_ireplace('<script', '<scr\'+\'ipt', $content);
		$content=str_ireplace('</script>', '</scr\'+\'ipt>', $content);
		
		$template=$this->pool->getTemplate();
		if($template && $template->hasRow()) {
			$templateContent=$template->getContent();
			$this->output=str_replace('%ADCONTENT%', $content, $templateContent);
		} else {
			/* Don't use document.write if we are using <script> tags */
			if(trim($content) != '') {
				$this->output=sprintf('document.write(\'%s\');', $content);
			}
		}
		echo $this->output;
		die();
	}
	
	protected function writeFrame() {
		return sprintf('<iframe src="http://%s/frame/?pools=%s&site=%s&timeout=%s&_=%s" scrolling="no" frameborder="0" allowtransparency="true" style="overflow:hidden;width:%spx;height:%spx;border:none;"></iframe>', 
				$_SERVER['HTTP_HOST'], join(',', $this->poolIds), $this->siteId, $this->pool->getRefreshRateMS(), time(), $this->type->getWidth(), $this->type->getHeight());
	}
}