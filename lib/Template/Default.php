<?php /* @var $this Widget_Site */ ?>
<?= $this->getSite()->getDocType(); ?>
<html>
	<head>
		<?= $this->printHeader(); ?>
	</head>
	
	<body>
		<div class="container">
			<div class="header">
				<a href="<?= \Pecee\Router::GetRoute('', ''); ?>"><img src="/gfx/logo.png" /></a>
				<?= $this->nav; ?>
			</div>
			<div class="ctn">
				<?= $this->getContentHtml(); ?>
			</div>
		</div>
	</body>
</html>