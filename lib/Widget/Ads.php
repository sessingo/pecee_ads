<?php
class Widget_Ads extends Widget_Site {
	protected $ads;
	protected $query;
	protected $siteId;
	protected $poolId;
	protected $page;
	protected $pagingFormat;
	public function __construct() {
		$this->navActiveIndex=1;
		parent::__construct();
		$this->getSite()->addWrappedJs('functions.js');
		
		if($this->hasParam('siteId')) {
			\Pecee\Session::Instance()->set('Ads_SiteID', $this->getParam('SiteID'));
		}
		$this->siteId = \Pecee\String::GetFirstOrValue($this->getParam('siteId', \Pecee\Session::Instance()->get('Ads_SiteID')),NULL);
		$this->poolId = $this->getParam('poolId');
		
		$this->query =  $this->getParam('query');
		$this->page = \Pecee\String::GetFirstOrValue($this->getParam('page'), 0);
		
		$this->ads = Model_Ad::Get(NULL, $this->query, $this->siteId, $this->poolId, 15, $this->page);
		
		/* Paging format */
		$this->pagingFormat = \Pecee\Router::GetRoute('ads', '', NULL, array('page' => '%1$d'));
	}
}