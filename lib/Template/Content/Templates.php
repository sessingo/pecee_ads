<?php /* @var $this Widget_Templates */ ?>
<h3><?= $this->_('Templates')?></h3>
<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP) ?>

<? if($this->templates->hasRows()) : ?>
<table cellspacing="0" cellpadding="0" width="100%" class="margin-top">
	<tr>
		<td class="bold padding-bottom" width="80%">
			<?= $this->_('Navn')?>
		</td>
		<td align="right" class="bold padding-bottom">
			<?= $this->_('Funktioner') ?>
		</td>
	</tr>
	<? foreach( $this->templates->getRows() as $template ) : ?>
	<tr>
		<td height=25">
			<?= $template->Name ?>
		</td>
		<td align="right">
			<a href="<?= \Pecee\Router::GetRoute('templates', 'edit', array($template->TemplateID))?>"><?= $this->_('Rediger')?></a>
			<span class="separator">|</span>
			<a onclick="return confirm('<?= $this->_('Dette sletter templaten fra systemet.\nEr du sikker på, at du vil fortsætte?'); ?>');" href="<?= \Pecee\Router::GetRoute('templates', 'delete', array($template->TemplateID))?>"><?= $this->_('Slet')?></a>
		</td>
	</tr>
	<? endforeach; ?>
</table>
<? endif; ?>