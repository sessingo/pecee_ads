<?php
class Model_Ad_Site extends \Pecee\Model\Model {
	public function __construct() {
		parent::__construct('Site', array('SiteID' => NULL, 'Name' => '', 'Url' => '', 'Visible' => '', 'Disabled' => FALSE));
	}
	
	public static function GetById($siteId) {
		return self::FetchOne('SELECT * FROM `Site` WHERE `SiteID` = %s', $siteId);
	}
	
	public static function Get($query=NULL, $visible=FALSE, $rows=10, $page=0) {
		$where = array('1=1');
		if(!is_null($visible)) {
			$where[] = \Pecee\DB\DB::FormatQuery('Visible = %s', array($visible));
		}
		if($query) {
			$where[] = 'Name LIKE \'%'.\Pecee\DB\DB::Escape($query).'%\' '.$where.' OR Url LIKE \''.\Pecee\DB\DB::Escape($query).'\' '.$where;
		}
		return self::FetchPage('SiteID', 'SELECT * FROM `Site` WHERE '.JOIN(' AND ', $where), $rows, $page);
	}
}