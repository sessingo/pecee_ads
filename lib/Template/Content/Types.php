<?php /* @var $this Widget_Pool_Type */ ?>
<h3><?= $this->_('Typer')?></h3>
<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP) ?>
<? if($this->types->hasRows()) : ?>
	<table cellspacing="0" width="100%" cellpadding="0" class="margin-top">
		<tr>
			<td width="85%" class="bold padding-bottom">
				<?= $this->_('Bredde')?> x <?= $this->_('Højde')?>
			</td>

			<td width="15%" class="bold padding-bottom" align="right">
				<?= $this->_('Funktioner') ?>
			</td>
		</tr>	
	<? /* @var $type Model_Pool_Type */
	foreach($this->types->getRows() as $type) : ?>
		<tr> 
			<td height="25">
				<?= $type->getWidth() ?>x<?= $type->getHeight(); ?>
			</td>
			<td align="right">
				<a href="<?= \Pecee\Router::GetRoute('types', 'edit', array($type->getPoolTypeID())) ?>"><?= $this->_('Rediger') ?></a>
				<span class="separator">|</span>
				<a onclick="return confirm('<?= $this->_('Er du sikker på, at du vil slette denne type?');?>');" href="<?= \Pecee\Router::GetRoute('types', 'delete', array($type->getPoolTypeID())) ?>"><?= $this->_('Slet')?></a>
			</td>
		</tr>
	<? endforeach; ?>
	</table>
<? endif; ?>