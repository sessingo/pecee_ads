<?php
class Widget_Log extends Widget_Site {
	protected $page;
	protected $messagesLimit;
	protected $logs;
	public function __construct() {
		$this->navActiveIndex=6;
		parent::__construct();
		
		$this->page = \Pecee\String::GetFirstOrValue($this->getParam('page'), 0);
		if($this->isPostBack()) {
			if($this->data->rows && $this->data->rows != '20' && \Pecee\Integer::is_int($this->data->rows)) {
				\Pecee\Session::Instance()->set('log_limit', $this->data->rows);
			}
		}
	
		$this->messagesLimit = \Pecee\Session::Instance()->get('log_limit', 20);
		$this->logs = Model_Ad_Log::Get($this->data->query, $this->messagesLimit, $this->page);
	}
}