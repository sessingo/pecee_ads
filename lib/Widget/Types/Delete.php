<?php
class Widget_Types_Delete extends Widget_Site {
	public function __construct($typeId) {
		parent::__construct();
		if(\Pecee\Integer::is_int($typeId)) {
			$type=Model_Pool_Type::GetById($typeId);
			if($type->hasRow()) {
				$type->delete();
				Model_Ad_Log::Log(sprintf('%s slettede typen %sx%s (%s)',
									\Pecee\Model\User\ModelUser::Current()->getUsername(),
									$type->getWidth(),
									$type->getHeight(),
									$typeId));
			}
		}
		\Pecee\Router::GoBack();
	}
}