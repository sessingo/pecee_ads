<?php
class Dataset_Types extends \Pecee\Dataset {
	public function __construct() {
		$this->createArray('', $this->_('-- Vælg --'));
		$types=Model_Pool_Type::Get(NULL, NULL, NULL, NULL);
		if($types->hasRows()) {
			foreach($types->getRows() as $type) {
				$this->createArray($type->getPoolTypeID(), $type->getWidth() . 'x' . $type->getHeight());
			}
		}
	}
	
}