<?php $query = (isset($_GET['query'])) ? $_GET['query'] : null; 
	$results = (isset($_GET['num'])) ? $_GET['num'] : 10;
	$pageIndex = (isset($_GET['p'])) ? $_GET['p'] : 0;
?>
<form method="get" action="">
	<input type="text" name="query" value="<?= htmlentities($query) ?>" />
	<input type="text" name="num" value="<?= $results ?>" />
	<input type="submit" value="Søg" />
</form>

<hr/>

<?php
	if( $query ) {
		$index = 0;
		$estimatedResults = 0;
		$pageIndex;
		$repeations = (($results > 0) ? ceil($results/4) : 0);

		for( $i=0;$i<$repeations;$i++ ) {
			
			$url = "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&start=".(($results*$pageIndex) + ($i*4))."&hl=da&q=" . urlencode($query);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_REFERER, 'http://search.jubii.dk');
			$body = curl_exec($ch);
			curl_close($ch);
			
			// now, process the JSON string
			$response = json_decode($body);
			$estimatedResults = $response->responseData->cursor->estimatedResultCount;
			if( $response->responseData->results ) {
				foreach( $response->responseData->results as $result ) : 
				
				if( $index >= $results ) break;   ?>
				<h3 style="margin-bottom:5px;margin-top:0px;">
					<a href="<?= $result->url?>"><?= (($results*$pageIndex) + ($index+1)) ?>: <?= utf8_decode($result->title)?></a>
				</h3>
				<?= utf8_decode($result->content)?>
				<div>
					<?= $result->visibleUrl?> - 
					<a href="<?= $result->cacheUrl?>">Cached</a>
				</div><hr/>
				<? 
				$index++;
				endforeach;
			}		
		}
		$maxPageIndex = ceil( $estimatedResults / $results );
		
		if( $pageIndex > 0 ) {
			echo '<a href="?query='.$query.'&num='.$results.'&p='.($pageIndex-1).'">forrige</a>&nbsp;&nbsp;';
		}
		
		if( $pageIndex <= $maxPageIndex ) {
			echo '<a href="?query='.$query.'&num='.$results.'&p='.($pageIndex+1).'">næste</a>';
		}
		
		
	}

?>