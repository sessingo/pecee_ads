<?php
class Site {
	protected static $instance;
	public $cache;
	public static function Instance() {
		if(is_null(self::$instance)) {
			self::$instance=new self();
		}
		return self::$instance;
	}
	
	public function __construct() {
		$this->cache=new Site_Cache();
	}
}