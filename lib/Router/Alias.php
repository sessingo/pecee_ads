<?php
class Router_Alias extends \Pecee\Router\RouterAlias {
	public function getPath($currentPath) {
		$path = @explode('/', $currentPath);
		if(isset($path[0])) {
			switch(strtolower($path[0])) {
				case 'positions':
					if(isset($path[1]) && $path[1] == 'js')
						return \Pecee\Router::GetRoute('ads', 'js', NULL, NULL, TRUE, TRUE);
					break;
				case 'click':
					return \Pecee\Router::GetRoute('default', 'click', NULL, NULL, TRUE, TRUE);
					break;
				case 'js':
					if(!isset($path[1]) || isset($path[1]) && strtolower($path[1]) != 'wrap') {
						return \Pecee\Router::GetRoute('default', 'js', NULL, NULL, TRUE, TRUE);
					}
					break;
				case 'login':
					return \Pecee\Router::GetRoute('default', 'login', NULL, NULL, TRUE, TRUE);
					break;
				case 'logout':
					return \Pecee\Router::GetRoute('default', 'logout', NULL, NULL, TRUE, TRUE);
					break;
				case 'contact':
					return \Pecee\Router::GetRoute('default', 'contact', NULL, NULL, TRUE, TRUE);
					break;
				case 'log':
					return \Pecee\Router::GetRoute('default', 'log', NULL, NULL, TRUE, TRUE);
					break;
				case 'click':
					return \Pecee\Router::GetRoute('default', 'click', array($path), NULL, NULL, TRUE);
					break;
				case 'frame':
					return \Pecee\Router::GetRoute('show', 'frame', NULL, NULL, TRUE, TRUE);
					break;
				case 'ad':
					return \Pecee\Router::GetRoute('show', 'index', NULL, NULL, TRUE, TRUE);
					break;
			}
		}
		return $currentPath;
	}
	
	public function getUrl($currentUrl) {
		return $currentUrl;
	}
}