<?php
class Widget_Templates_Delete extends Widget_Site {
	public function __construct($templateId) {
		parent::__construct();
		
		$template=Model_Ad_Template::GetById($templateId);
		if($template->hasRow()) {
			$template->delete();
		}
		\Pecee\Router::GoBack();
	}
}