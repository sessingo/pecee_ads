<?php
class Widget_Ads_Show extends Widget_Site {
	protected $SiteID;
	protected $UniqueTag;
	protected $ad;
	protected $size;
	
	public function __construct($adId) {
		$this->navActiveIndex=1;
		parent::__construct();
		$this->ad = Model_Ad::GetById($adId);
	}
}