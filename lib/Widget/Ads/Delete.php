<?php
class Widget_Ads_Delete extends Widget_Site {
	public function __construct($adId) {
		parent::__construct();
		if(\Pecee\Integer::is_int($adId)) {
			$ad=Model_Ad::GetById($adId);
			if($ad->hasRow()) {
				$ad->delete();
				$ad->clearCache();
				Model_Ad_Log::Log(sprintf('%s slettede reklamen %s (%s)', \Pecee\Model\User\ModelUser::Current()->getUsername(), $ad->getName(), $adId));
			}
			
		}
		\Pecee\Router::Redirect(\Pecee\Router::GetRoute('ads', ''));		
	}
}