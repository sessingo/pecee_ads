<?php
class Controller_Pools extends \Pecee\Controller\Controller {
	
	private function showMenu() {
		$menu = new \Pecee\UI\Menu\Menu();
		$menu->setClass('menu');
		$menu->addItem($this->_('Ny pool'), \Pecee\Router::GetRoute(NULL, 'new'));
		$menu->addItem($this->_('Oversigt'), \Pecee\Router::GetRoute(NULL, ''));
		\Pecee\UI\Site::GetInstance()->addToLocation($menu, \Pecee\UI\Site::LOCATION_TOP);
	}
	
	public function indexView() {
		$this->showMenu();
		echo new Widget_Pools();
	}
	
	public function newView() {
		$this->showMenu();
		echo new Widget_Pools_New();
	}
	
	public function editView($poolId) {
		$this->showMenu();
		echo new Widget_Pools_Edit($poolId);
	}
	
	public function deleteView($poolId) {
		echo new Widget_Pools_Delete($poolId);
	}
}