<?php
class Widget_Positions_Delete extends Widget_Site {
	public function __construct($positionId) {
		parent::__construct();
		if(\Pecee\Integer::is_int($positionId)) {
			$position=Model_Ad_Position::GetById($positionId);
			$position->delete();
			Model_Ad_Log::Log(sprintf('%s slettede %s', \Pecee\Model\User\ModelUser::Current()->getUsername(), $positionId));
		}
		\Pecee\Router::GoBack();
	}
}