<?php
class Widget_Templates_Preview extends Widget_Site {
	protected $templateContent;
	protected $tags=array('%ADCONTENT%', '%UNIQUETAG%', '%POSITIONID%', '%SITEID%', '%TYPEID%', '%TYPEWIDTH%', '%TYPEHEIGHT%');
	public function __construct() {
		$this->navActiveIndex=3;
		parent::__construct();
		$this->setTemplate(NULL);
		$template = $this->getParam('template',FALSE);
		$template = ($template) ? \Pecee\String\Encoding::Base64Decode($template) : $template;
		if($template instanceof Model_Ad_Template) {
			$site = Model_Ad_Site::GetById($template->getPool()->getSiteID());
			if(!$site->hasRow()) {
				\Pecee\Router::Redirect(\Pecee\Router::GetRoute('templates', ''));
			}
			
			if(!$content = \Pecee\Curl::Download($site->getUrl())) {
				$this->setError($this->_('Siden kunne ikke indlæses og er muligvis nede'));
				\Pecee\Router::Redirect(\Pecee\Router::GetRoute('templates', ''));
			}
			if(!$this->hasErrors()) {					
				$content = preg_replace('#(href|src)="/([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#', '$1="'.trim($site->getUrl(), '/').'/$2$3', $content);
				$content .= sprintf('<script type="text/javascript">%s</script>', $template->getContent());
				$this->templateContent = $content;
			}		
		}
	}
}