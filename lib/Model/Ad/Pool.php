<?php
class Model_Ad_Pool extends \Pecee\Model\Model {
	public function __construct() {
		parent::__construct('AdPool', array('AdPoolID' => NULL, 'AdID' => NULL, 'PoolID' => NULL));
	}
	
	public static function Clear($adId) {
		self::NonQuery('DELETE FROM `AdPool` WHERE `AdID` = %s', $adId);
	}
}