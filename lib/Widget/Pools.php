<?php
class Widget_Pools extends Widget_Site {
	protected $pools;
	protected $siteId;
	protected $query;
	public function __construct() {
		$this->navActiveIndex=3;
		parent::__construct();
		$this->siteId=$this->getParam('siteId');
		$this->query=$this->getParam('query');
		$this->pools=Model_Pool::Get($this->query, $this->siteId, 30, NULL);
	}
}