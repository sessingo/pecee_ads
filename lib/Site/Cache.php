<?php
class Site_Cache {
	public function get($siteId, $key) {
		$cache=\Pecee\Service\Memcache\Memcache::GetInstance();
		$ns=$cache->get(self::GetNamespace($siteId));
		return is_array($ns) && isset($ns[$key]) ? $ns[$key] : NULL;
	}
	public function set($siteId, $key, $value, $minutes) {
		$cache=\Pecee\Service\Memcache\Memcache::GetInstance();
		$ns=$cache->get(self::GetNamespace($siteId));
		$ns=(!is_array($ns)) ? array() : $ns;
		$ns[$key]=$value;
		
		$cache->getMemcache()->set(self::GetNamespace($siteId), $ns, NULL, 60*$minutes);
	}
	public function clear($siteId) {
		\Pecee\Service\Memcache\Memcache::GetInstance()->getMemcache()->set(self::GetNamespace($siteId), NULL);
	}
	
	private static function GetNamespace($siteId) {
		return 'Ads_'.$siteId;
	}
}