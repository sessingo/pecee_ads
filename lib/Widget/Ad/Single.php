<?php
class Widget_Ad_Single extends \Pecee\Widget\Widget {
	protected $ad;
	protected $output;
	public function __construct($site, array $pools) {
		parent::__construct();
		$this->setTemplate('');
		$this->output='';
		if(!$site || !is_array($pools)) {
			return;
		}
		
		$poolIds=(is_array($pools)) ? $pools : explode(',', $pools);
		
		$key=sprintf('ADS_%s', md5(join('_',$poolIds) . $site));
		$ads=Site::Instance()->cache->get($site, $key);
		if(!$ads) {
			$ads=Model_Ad::GetByPoolId($site, $poolIds);
			if($ads) {
				Site::Instance()->cache->set($site, $key, $ads, 60*24*7);
			}
		}
				
		$this->ad = $ads->getRandom();
		if($this->ad instanceof Model_Ad && $this->ad->hasRow()) {
			if($this->ad->MaxViews > 0 && $this->ad->MaxViews <= ($this->ad->Views + 1)) {
				$this->ad->setDisabled(TRUE);
				$this->ad->update();
			}
		
			// Register view
			$this->ad->registerView();
			$this->output= Helper::WriteAd($this->ad);
		}
	}
}