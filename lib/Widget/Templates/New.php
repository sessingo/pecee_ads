<?php
class Widget_Templates_New extends Widget_Site {
	protected $siteId;
	protected $showPreviewButton;
	protected $template;
	public function __construct($siteId=NULL) {
		$this->navActiveIndex=2;
		parent::__construct();
		$this->siteId = $siteId;
		$this->template=new Model_Ad_Template();
		
		if($this->isPostBack()) {
			
			$this->template=\Pecee\String\Encoding::Base64Decode($this->data->template, $this->template);	
			$this->template->setName($this->data->Name);
			$this->template->setContent($this->data->Content);
			$this->template->setPoolID($this->data->PoolID);
			if($this->data->previewPostBack) {
				if($this->data->PoolID) {
					$this->showPreviewButton = TRUE;
				}
			} elseif($this->data->autoPostBack == '0') {
				$this->addInputValidation($this->_('Navn'), 'Name', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
				$this->addInputValidation($this->_('Javascript'), 'content', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
			
				if(!$this->hasErrors()) {
					
					$this->template->save();
					// TODO: insert logging ?
					\Pecee\Router::Redirect(\Pecee\Router::GetRoute('templates', ''));
				}
			}
		}
	}
}