<?php
class Dataset_User_AdminLevel extends \Pecee\Dataset {
	
	public function __construct() {
		$this->createArray('',$this->_('-- Vælg --'));
		$this->createArray('1', $this->_('Partner'));
		$this->createArray('2',$this->_('Administrator'));		
	}
	
}