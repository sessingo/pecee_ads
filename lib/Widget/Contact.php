<?php
class Widget_Contact extends Widget_Site  {
	protected $selectedSiteID;	
	public function __construct( $SiteID = NULL) {
		$this->navActiveIndex=1;
		if(\Pecee\Model\User\ModelUser::IsLoggedIn()) {
			if(\Pecee\Model\User\ModelUser::Current()->getAdminLevel() == 2) {
				$this->navActiveIndex=8;
			} else {
				$this->navActiveIndex=2;
			}
		}
		parent::__construct(NULL);
		
		$this->selectedSiteID = $SiteID;
		$this->prependSiteTitle($this->_('Kontakt') . ' - ');
		
		if( $this->isPostBack() && $this->data->autoPostBack != '1' ) {
			$this->addInputValidation($this->_('Navn'), 'Name', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
			//$this->addInputValidation($this->_('Efternavn'), 'Lastname', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
			$this->addInputValidation($this->_('E-mail'), 'Email', new \Pecee\UI\Form\Validate\ValidateInputEmail());
			//$this->addInputValidation($this->_('Hjemmeside'), 'Homepage', new \Pecee\UI\Form\Validate\ValidateInputUri());
			$this->addInputValidation($this->_('Besked'), 'Text', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
			$this->addInputValidation($this->_('Verificeringsnøgle'), 'captchaText', new \Pecee\UI\Form\Validate\ValidateInputCaptcha('contactCaptcha'));
			
			if( !$this->hasErrors() ) {
				mail( 'ss@pecee.dk', 'Henvendelse fra ads.pecee.dk', 
				"Reklame-site: " . $this->data->SiteID .
				"\n\nNavn. " . $this->data->Name .
				"\nEmail: ". $this->data->Email.
				"\nHjemmeside: ". $this->data->Homepage .
				"\nVedkommende har hørt om ads.pecee.dk: " . $this->data->aboutYou .
				"\nHenvendelse:\n" . $this->data->Text );
				$this->setMessage($this->_('Tak for din henvendelse - vi vender tilbage hurtigst muligt.'), 'MessageSent');
				\Pecee\Router::Refresh();
			}
		}		
	}
	
}