<?php
class Controller_Types extends \Pecee\Controller\Controller {
	
	private function ShowMenu() {
		$menu = new \Pecee\UI\Menu\Menu();
		$menu->setClass('menu');
		$menu->addItem($this->_('Ny type'), \Pecee\Router::GetRoute('types', 'new'));
		$menu->addItem($this->_('Oversigt'), \Pecee\Router::GetRoute('types', ''));
		\Pecee\UI\Site::GetInstance()->addToLocation($menu, \Pecee\UI\Site::LOCATION_TOP);
	}
	
	public function indexView() {
		$this->ShowMenu();
		$widget = new Widget_Types();
		echo $widget;
	}
	
	public function newView() {
		$this->ShowMenu();
		$widget = new Widget_Types_New();
		echo $widget;
	}
	
	public function editView($TypeID) {
		$this->ShowMenu();
		$widget = new Widget_Types_Edit($TypeID);
		echo $widget;
	}
	
	public function deleteView($TypeID) {
		$this->ShowMenu();
		$widget = new Widget_Types_Delete($TypeID);
		echo $widget;
	}
	
}