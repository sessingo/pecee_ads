<?php
class Model_Ad_Position_Rule extends \Pecee\Model\Model {
	public function __construct() {
		parent::__construct('PositionRule', array('PositionRuleID' => NULL, 'PositionID' => '', 'Rule' => '', 'Allowed' => TRUE));
	}
	
	/**
	 * Get rules by rule position id
	 * @param int $positionId
	 * @return Model_Ad_Position_Rule
	 */
	public static function GetById($positionRuleId) {
		return self::FetchOne('SELECT * FROM `PositionRule` WHERE `PositionRuleID` = %s', $positionRuleId);
	}
	
	public static function GetBySiteId($siteId, $width, $height) {
		$key=sprintf('PositionRule_%s', md5($width . $height));
		$rules=Site::Instance()->cache->get(Site_Cache::GetNamespace($siteId), $key);
		if(!$rules) {
			$where=array('p.`Disabled` = 0', \Pecee\DB\DB::FormatQuery('t.Width = %s AND t.Height = %s', array($width, $height)));
			$where[]=\Pecee\DB\DB::FormatQuery('p.`SiteID` = %s', array($siteId));
			$rules = self::FetchAll('SELECT pr.* FROM `Position` p LEFT JOIN `PositionRule` pr ON(pr.`PositionID` = p.`PositionID`) LEFT JOIN `Type` t ON(t.`TypeID` = p.`TypeID`) WHERE ' . join(' AND ', $where));
			if($rules) {
				Site::Instance()->cache->set(Site_Cache::GetNamespace($siteId), $key, $rules, 60*24*7);
			}
		}
		return $rules;
	}
	
	/**
	 * Get rules by position id
	 * @param int $positionId
	 * @return Model_Ad_Position_Rule
	 */
	public static function GetByPositionId($positionId) {
		return self::FetchAll('SELECT * FROM `PositionRule` WHERE `PositionID` = %s', $positionId);
	}
	
	public static function Clear($positionId) {
		return self::NonQuery('DELETE FROM `PositionRule` WHERE `PositionID` = %s', $positionId);
	}
}