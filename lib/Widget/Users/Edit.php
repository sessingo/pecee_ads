<?php
class Widget_Users_Edit extends Widget_Site {
	protected $user;
	public function __construct($userId) {
		$this->navActiveIndex=7;
		parent::__construct();
		$this->user = \Pecee\Model\User\ModelUser::GetByUserID($userId);
		
		$this->addInputValidation($this->_('Brugernavn'), 'username', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
		$this->addInputValidation($this->_('Adgangskode'), 'password', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
		$this->addInputValidation($this->_('Gentag adgangskode'), 'passwordRepeat', array(new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty(), new \Pecee\UI\Form\Validate\ValidateInputRepeation($this->_('Adgangskode'), $this->data->password)));
		$this->addInputValidation($this->_('E-mail'), 'email', new \Pecee\UI\Form\Validate\ValidateInputEmail());
		$this->addInputValidation($this->_('Bruger type'), 'UserAdminLevel', array(new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty(), new \Pecee\UI\Form\Validate\ValidateInputInteger()));	
		
		if($this->isPostBack() && !$this->hasErrors()) {
			$this->user->setUsername($this->data->Username);
			$this->user->setPassword($this->data->Password);
			$this->user->setEmail($this->data->Email);
			$this->user->setAuthLevel($this->data->UserAdminLevel);
			$this->user->update();
			\Pecee\Router::GoBack();
		}
		
	}
}