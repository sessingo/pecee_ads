<?php
class Dataset_Positions extends \Pecee\Dataset {
	public function __construct($choose=NULL, $siteId=NULL, $visible=NULL) {
		if(!is_null($choose)) {
			$this->createArray('', $choose);
		}
		$positions = Model_Ad_Position::GetBySiteId($siteId, $visible);
		if($positions->hasRows()) {
			foreach($positions->getRows() as $position) {
				$this->createArray($position->PositionID, $position->Name);
			}
		}
	}
}