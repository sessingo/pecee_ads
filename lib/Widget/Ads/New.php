<?php
class Widget_Ads_New extends Widget_Site {
	protected $pools;
	protected $poolIdsToExclude;
	protected $siteId;
	protected $isHtml;
	public function __construct($siteId=NULL) {
		$this->navActiveIndex=1;
		parent::__construct();
		$this->poolIdsToExclude=array();
		
		if($this->isPostBack()) {
			$this->isHtml=$this->data->isHtml;
			$this->pools=\Pecee\String\Encoding::Base64Decode($this->data->pools, array());			
			
			if($this->data->autoPostBack != '1') {
				$this->addInputValidation($this->_('Navn'), 'name', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
				if(!$this->isHtml) {
					$this->addInputValidation($this->_('Klik url'), 'url', new \Pecee\UI\Form\Validate\ValidateInputUri());
				}
	
				$this->addInputValidation($this->_('Udløbs dato'), 'expiresDate', new \Pecee\UI\Form\Validate\ValidateInputDate(TRUE));
				$this->addInputValidation($this->_('Indhold'), 'content', (($this->isHtml) ? new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty() : new \Pecee\UI\Form\Validate\ValidateInputUri()));
				$this->addInputValidation($this->_('Visnings procent'), 'showPercentage', new \Pecee\UI\Form\Validate\ValidateInputInteger());
			
				if(!$this->hasErrors()) {
					$ad=new Model_Ad();
					$ad->UserID=\Pecee\Model\User\ModelUser::Current()->getUserID();
					$ad->Name=$this->data->name;
					$ad->Url=$this->data->url;
					$ad->Content=$this->data->content;
					$ad->ShowPercentage=$this->data->showPercentage;
					$ad->IsHtml=$this->isHtml;
					$ad->ExpiresDate=($this->data->expiresDate) ? \Pecee\Date::FromToDateTime($this->data->expiresDate) : NULL;
					$ad->MaxViews=($this->data->maxViews) ?$this->data->MaxViews : NULL;
					$ad->Disabled=(!$this->data->active);
					$ad->setPools($this->pools);					
					$ad->save();
					
					/* Clear cache */
					$ad->clearCache();
					
					Model_Ad_Log::Log(sprintf('%s opretted en ny reklame: %s (%s)', \Pecee\Model\User\ModelUser::Current()->getUsername(), $this->data->Name, $ad->getAdID()));
					\Pecee\Router::Redirect(\Pecee\Router::GetRoute('ads', ''));
				}
			} else {
				if($this->data->addPool && $this->data->poolId) {
					$this->pools[]=Model_Pool::GetById($this->data->poolId);
				} elseif(\Pecee\Integer::is_int($this->data->poolToDelete)) {
					unset($this->pools[$this->data->poolToDelete]);
				}
			}
			
			if(count($this->pools) > 0) {
				foreach($this->pools as $pool) {
					if($pool instanceof Model_Pool) {
						if(!in_array($pool->getPoolID(), $this->poolIdsToExclude)) {
							$this->poolIdsToExclude[]=$pool->getPoolID();
						}
					}
				}
			}
		} else {
			$this->isHtml=TRUE;
			$this->pools=array();
		}
	}
}