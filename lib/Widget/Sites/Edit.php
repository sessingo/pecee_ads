<?php
class Widget_Sites_Edit extends Widget_Site {
	protected $site;
	public function __construct($siteId) {
		$this->navActiveIndex=5;
		parent::__construct();
		if(\Pecee\Integer::is_int($siteId)) {
			$this->site = Model_Ad_Site::GetById($siteId);
			$this->addInputValidation($this->_('Navn'), 'Name', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
			$this->addInputValidation($this->_('Url'), 'Url', new \Pecee\UI\Form\Validate\ValidateInputUri());
			
			if($this->isPostBack() && !$this->hasErrors()) {
				$this->site->setName($this->data->Name);
				$this->site->setUrl($this->data->Url);
				$this->site->setVisible(($this->data->Visible));
				$this->site->setDisabled(!($this->data->Disabled));
				$this->site->update();
				
				Model_Ad_Log::Log(sprintf('%s opdaterede %s (%s) til %s (%s)', 
									\Pecee\Model\User\ModelUser::Current()->getUsername(), 
									$this->site->Name,
									$this->site->Url,
									$this->data->Name,
									$this->data->Url));
				
				\Pecee\Router::Redirect(\Pecee\Router::GetRoute('sites', ''));
			}
		} else {
			\Pecee\Router::Redirect(\Pecee\Router::GetRoute('sites', ''));
		}
	}
}