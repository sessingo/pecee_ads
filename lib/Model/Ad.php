<?php
class Model_Ad extends \Pecee\Model\Model {
	protected $pools;
	public function __construct() {
		parent::__construct('Ad', array('AdID' => NULL, 'UserID' => NULL, 'Name' => NULL,
										'Url' => NULL, 'Content' => NULL, 'ShowPercentage' => NULL, 'IsHtml' => FALSE,
										'ExpiresDate' => NULL, 'Views' => 0, 'UniqueViews' => 0, 'Clicks' => 0,
										'UniqueClicks' => 0, 'MaxViews' => NULL, 'Disabled' => FALSE, 'Deleted' => FALSE,
										'CreatedDate' => \Pecee\Date::ToDateTime(time())));
	}
	
	/**
	 * Get ad pool
	 * @return array|NULL
	 */
	public function getPools() {
		if($this->pools) {
			return $this->pools;
		}
		$pools= Model_Pool::GetByAdId($this->AdID);
		if($pools->hasRows()) {
			return $pools->getRows();
		}
		return NULL;
	}
	
	public function setPools(array $pools) {
		$this->pools=$pools;
	}
	
	public function clearCache() {
		if($this->getPools()) {
			foreach($this->getPools() as $pool) {
				Site::Instance()->cache->clear($pool->getSiteID());
			}
		}
	}
	
	protected function updatePools() {
		if($this->pools && count($this->pools) > 0) {
			Model_Ad_Pool::Clear($this->AdID);
			foreach($this->pools as $pool) {
				if($pool instanceof Model_Pool) {
					$p=new Model_Ad_Pool();
					$p->setAdID($this->AdID);
					$p->setPoolID($pool->getPoolID());
					$p->save();
				}
			}
		}
	}
	
	public function save() {
		$this->AdID=parent::save()->getInsertId();
		$this->updatePools();
		//$this->clearCache();
	}
	
	public function update() {
		parent::update();
		$this->updatePools();
		//$this->clearCache();
	}
	
	public function delete() {
		$this->Deleted=TRUE;
		$this->update();
	}
	
	public function registerView() {
		$view=Model_Ad_View::Find($this->AdID, \Pecee\Server::GetRemoteAddr());
		if(!$view->hasRow()) {
			$this->UniqueViews+=1;
		}
		$view=new Model_Ad_View();
		$view->setAdID($this->AdID);
		$view->setIPAddress(\Pecee\Server::GetRemoteAddr());
		$view->save();
		$this->Views+=1;
		$this->update();
	}
	
	public function registerClick() {
		$click=Model_Ad_Click::Find($this->AdID, \Pecee\Server::GetRemoteAddr());
		if(!$click->hasRow()) {
			$this->UniqueClicks+=1;
		}
		$click=new Model_Ad_Click();
		$click->setAdID($this->AdID);
		$click->setIPAddress(\Pecee\Server::GetRemoteAddr());
		$click->save();
		$this->Clicks+=1;
		$this->update();
	}
	
	public function getRandom() {
		if($this->hasRows()) {
			$adsArray = array();
			/* @var $ad Model_Ad */
			foreach($this->getRows() as $ad) {
				$percentage = (strlen($ad->ShowPercentage) > 1) ? round(min($ad->ShowPercentage,100)) : round(min($ad->ShowPercentage, 100));
				for($i=0;$i<$percentage;$i++) {
					$adsArray[] = $ad;
				}
			}
			if(count($adsArray) > 0) {
				$maxAttemps=10;
				$i=0;
				while($i < $maxAttemps) {
					$rand = mt_rand(0, count($adsArray));
					if(isset($adsArray[$rand])) {
						return $adsArray[$rand];
					}
					$i++;
				}
			}
		}
		return NULL;
	}
	
	/**
	 * Get ad by id
	 * @param int $adId
	 * @return Model_Ad
	 */
	public static function GetById($adId) {
		return self::FetchOne('SELECT * FROM Ad WHERE AdID = %s', $adId);
	}
	
	public static function Get($userId=NULL, $query=NULL, $siteId=NULL, $poolId=NULL, $rows=10, $page=0, $order='Disabled ASC, AdID DESC') {
		$where = array('Deleted = 0');
		if($query) {
			$where[] = sprintf('(`Name` LIKE \'%%%1$s%%\' OR `Url` LIKE \'%%%1$s%%\' OR `Content` LIKE \'%%%1$s%%\')', \Pecee\DB\DB::Escape($query));
		}
		if(!is_null($siteId)) {	
			$where[] = \Pecee\DB\DB::FormatQuery('`AdID` IN (SELECT ap.AdID FROM AdPool ap JOIN `Pool` p ON(ap.`PoolID` = p.`PoolID`) JOIN `Site` s ON(s.`SiteID` = p.`SiteID`) WHERE s.`Disabled` = 0 && p.`SiteID` = %s)', array($siteId));
		}
		if(!is_null($poolId)) {
			$where[] = \Pecee\DB\DB::FormatQuery('`AdID` IN (SELECT ap.AdID FROM AdPool ap JOIN `Pool` p ON(ap.`PoolID` = p.`PoolID`) JOIN `Site` s ON(s.`SiteID` = p.`SiteID`) WHERE s.`Disabled` = 0 && p.`PoolID` = %s)', array($poolId));
		}
		if(!is_null($userId)) {
			$where[] = \Pecee\DB\DB::FormatQuery('`UserID` = %s', array($userId));
		}
		return self::FetchPage('`AdID`', 'SELECT * FROM Ad WHERE '.join(' AND ', $where).' ORDER BY '.$order, $rows, $page);
	}
	
	/**
	 * Get ads by siteid and pool id
	 * @param int $siteId
	 * @param int $poolId
	 * @return Model_Ad
	 */
	public static function GetByPoolId($siteId, array $poolId) {
		return self::FetchAll('SELECT *, ap.`PoolID` FROM `Ad` a JOIN `AdPool` ap ON(ap.`AdID` = a.`AdID`) JOIN `Pool` p ON(p.`PoolID` = ap.`PoolID`) 
								JOIN `Site` s ON(s.`SiteID` = p.`SiteID`)
								WHERE s.`Disabled` = 0 && ap.`PoolID` IN('.\Pecee\DB\DB::JoinArray($poolId).') AND a.`Disabled` = 0 AND a.`Deleted` = 0');
	}
}