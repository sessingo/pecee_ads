<?php
class Dataset_HeardFromUs extends \Pecee\Dataset {
	
	public function __construct() {
		$this->createArray(0,$this->_('-- Vælg venligst --'));
		$this->createArray(1,$this->_('På en hjemmeside'));
		$this->createArray(2,$this->_('Gennem en ven eller kollega'));
		$this->createArray(3,$this->_('Via en reklame'));
		$this->createArray(4,$this->_('Gennem et nyhedsbrev'));
		$this->createArray(5,$this->_('Andet'));
	}
	
}