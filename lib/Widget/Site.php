<?php
abstract class Widget_Site extends \Pecee\Widget\Widget {
	protected $nav;
	protected $navActiveIndex=0;
	public function __construct($adminLevel=2) {
		$this->setAuthLevel($adminLevel);
		parent::__construct();
		
		$this->getSite()->addWrappedJs('jquery-1.7.2.min.js');
		$this->getSite()->addWrappedCss('style.css');
		$this->getSite()->setDebug(FALSE);
		$this->getSite()->setTitle('[P] Ads|Administration');
		
		$this->nav = new \Pecee\UI\Menu\Menu();
		$this->nav->setClass('menu');
		$this->nav->addItem($this->_('Forside'), \Pecee\Router::GetRoute('', ''));
		if(\Pecee\Model\User\ModelUser::IsLoggedIn()) {
			switch(\Pecee\Model\User\ModelUser::Current()->getAdminLevel()) {
				case 1:
					$this->nav->addItem($this->_('Reklamer'), \Pecee\Router::GetRoute('ads', ''));
					break;
				case 2:
					$this->nav->addItem($this->_('Reklamer'), \Pecee\Router::GetRoute('ads', ''));
					$this->nav->addItem($this->_('Templates'), \Pecee\Router::GetRoute('templates', ''));
					$this->nav->addItem($this->_('Pools'), \Pecee\Router::GetRoute('pools', ''));
					$this->nav->addItem($this->_('Typer'), \Pecee\Router::GetRoute('types', ''));
					$this->nav->addItem($this->_('Sider'), \Pecee\Router::GetRoute('sites', ''));
					$this->nav->addItem($this->_('Log'), \Pecee\Router::GetRoute('log', ''));
					$this->nav->addItem($this->_('Brugere'), \Pecee\Router::GetRoute('users', ''));
					break;
			}
			$this->nav->addItem($this->_('Kontakt'), \Pecee\Router::GetRoute('', 'contact'));
			$this->nav->addItem($this->_('Log ud'), \Pecee\Router::GetRoute('', 'logout'));
		} else {
			$this->nav->addItem($this->_('Kontakt'), \Pecee\Router::GetRoute('', 'contact'));
		}
		$this->nav->getItem($this->navActiveIndex)->addClass('active');
	}
}