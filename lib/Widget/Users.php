<?php
class Widget_Users extends Widget_Site {
	protected $users;
	public function __construct($page = 0) {
		$this->navActiveIndex=7;
		parent::__construct();
		$this->users = \Pecee\Model\User\ModelUser::Get(NULL, NULL, NULL, 'Username ASC', 15, $page);
	}
	
	protected function getAdminTitle( $AdminLevel ) {
		switch( $AdminLevel ) {
			default:
			case 1:
				return $this->_('Partner');
				break;
			case 2:
				return $this->_('Administrator');
				break;
		}
	}
}