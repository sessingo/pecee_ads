<?php
class Widget_Ad_Frame extends Pecee\Widget\Widget {
	protected $refreshRate;
	protected $siteId;
	protected $pools;
	public function __construct() {
		parent::__construct();
		$this->setTemplate('Iframe.php');
		
		$this->siteId=$this->getParam('site', FALSE);
		$this->pools=$this->getParam('pools', FALSE);
		$this->refreshRate=$this->getParam('timeout');
		
		if(!$this->siteId || !$this->pools) {
			die();
		}
		
		if($this->refreshRate=='') {
			$this->refreshRate=60000;
		}
		$this->pools=explode(',', $this->pools);
	}
}