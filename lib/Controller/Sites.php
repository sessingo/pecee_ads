<?php
class Controller_Sites extends \Pecee\Controller\Controller {
	
	private function ShowMenu() {
		$menu = new \Pecee\UI\Menu\Menu();
		$menu->setClass('menu');
		$menu->addItem($this->_('Ny side'), \Pecee\Router::GetRoute('sites', 'new'));
		$menu->addItem($this->_('Oversigt'), \Pecee\Router::GetRoute('sites', ''));
		\Pecee\UI\Site::GetInstance()->addToLocation($menu, \Pecee\UI\Site::LOCATION_TOP);
	}
	
	public function indexView() {
		$this->ShowMenu();
		$widget = new Widget_Sites();
		echo $widget;
	}
	
	public function newView() {
		$this->ShowMenu();
		$widget = new Widget_Sites_New();
		echo $widget;
	}
	
	public function editView($SiteID) {
		$this->ShowMenu();
		$widget = new Widget_Sites_Edit($SiteID);
		echo $widget;
	}
	
	public function deleteView($SiteID) {
		$this->ShowMenu();
		$widget = new Widget_Sites_Delete($SiteID);
		echo $widget;
	}
	
	public function refreshView($siteId) {
		Site::Instance()->cache->clear($siteId);
		\Pecee\Router::GoBack();
	}
	
}