<?php
require_once '../config/init.php';

\Pecee\Auth::GetInstance()->setAdminIP('127.0.0.1');

$router=\Pecee\Router::GetInstance();
$router->addAlias(new Router_Alias());
try {
	$router->routeRequest();
}catch(\Pecee\Auth\AuthException $e) {
	\Pecee\Router::Redirect(\Pecee\Router::GetRoute('login', ''));
}