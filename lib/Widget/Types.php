<?php
class Widget_Types extends Widget_Site {
	protected $types;
	public function __construct() {
		$this->navActiveIndex=4;
		parent::__construct();
		$width = ($this->data->width) ? $this->data->width : null;
		$height = ($this->data->height) ? $this->data->height : null;
 		$this->types = Model_Pool_Type::Get($width, $height, 10, 0);
	}
}