<?php /* @var $this Widget_Contact */ ?>
<h3><?= $this->_('Kontakt Pecee Ads')?></h3>


<? if($this->getMessage('MessageSent')) : ?>
	<div style="font-weight:bold;font-size:15px;"><?= $this->getMessage('MessageSent')->getMessage(); ?></div>
<? else: ?>
<p class="padding-bottom">
	<?= $this->_('Udfyld venligst den nedenstående formular. Så vender vi tilbage til dig hurtigst muligt.')?>
</p>

<?= $this->form()->start('contactForm', 'post', '')?>
<?= $this->form()->input('autoPostBack', 'hidden', '0')->addAttribute('ID', 'autoPostBack')?>
<?= $this->showErrors('contactForm'); ?>
<table width="100%" class="margin-top">
	<tr>
		<td width="20%" class="padding-top">
			<?= $this->_('Navn')?>
		</td>
		
		<td class="padding-top">
			<?= $this->form()->input('Name', 'text')?>
		</td>
	</tr>
	<tr>
		<td width="20%" class="padding-top">
			<?= $this->_('E-mail')?>
		</td>
		
		<td class="padding-top">
			<?= $this->form()->input('Email', 'text')?>
		</td>
	</tr>
	<tr>
		<td width="20%" class="padding-top">
			<?= $this->_('Hjemmeside')?>
		</td>
		
		<td class="padding-top">
			<?= $this->form()->input('Homepage', 'text', '', TRUE)?>
		</td>
	</tr>
	<tr>
		<td class="padding-top">
			<?= $this->_('Site') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->selectStart('SiteID', new Dataset_Sites($this->_('Ikke valgt'), TRUE), $this->selectedSiteID); ?>
		</td>
	</tr>
	<tr>
		<td class="padding-top" valign="top">
			<?= $this->_('Besked')?>
		</td>
		<td class="padding-top">
			<?= $this->form()->textarea('Text', 8, 50)?>
		</td>
	</tr>
	<tr>
		<td class="padding-top">
			<?= $this->_('Hvor har du hørt om os?')?>
		</td>
		<td class="padding-top">
			<?= $this->form()->selectStart('aboutYou', new Dataset_HeardFromUs(), 'test')?>
		</td>
	</tr>
	<tr>
		<td class="padding-top">
			<?= $this->_('Indtast verificeringskoden')?>
		</td>
		<td class="padding-top">
			<?= $this->form()->captcha('contactCaptcha')
				->setFontColor('#FFFFFF')
				->setFontShadowColor('#363636')
				->setFontSize(30)
				->setUniqueValueLength(5)
				->setFontShadowMarginLeft(13)
				->setFontMarginLeft(10)
				->setFontShadowSize(30)
				->addAttribute('class', 'margin-top')
				->addAttribute('style', 'margin-right:10px;vertical-align:top;');
			?>
			<?= $this->form()->input('captchaText', 'text')
				->addAttribute('style', 'margin-top:10px;') ?>
		</td>
	</tr>
	<tr>
		<td>
		</td>
		<td class="padding-top">
			<?= $this->form()->submit('submit', $this->_('Send'))->addAttribute('class', 'margin-top')?>
		</td>
	</tr>
</table>
<?php endif; ?>
<?= $this->form()->end(); ?>