<?php
class Widget_Positions_Edit extends Widget_Site {
	protected $position;
	protected $rules;
	public function __construct($positionId) {
		$this->navActiveIndex=3;
		parent::__construct();
		$this->position = Model_Ad_Position::GetById($positionId);
		
		if($this->isPostBack()) {
			$this->rules=\Pecee\String\Encoding::Base64Decode($this->data->data, array());
			if($this->data->addNewRule) {
				$this->addInputValidation($this->_('URL'), 'Rule', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
				if(!$this->hasErrors()) {
					$rule=new Model_Ad_Position_Rule();
					$rule->setRule($this->data->Rule);
					$rule->setAllowed($this->data->RuleAllowed);
					$this->rules[]=$rule;
				}
			} elseif(\Pecee\Integer::is_int($this->data->deleteRule)) {
				foreach($this->rules as $key=>$rule) {
					if($key==$this->data->deleteRule) {
						unset($this->rules[$key]);
					}
				}
			} else {
				$this->addInputValidation($this->_('Side'), 'SiteID', new \Pecee\UI\Form\Validate\ValidateInputInteger());
				$this->addInputValidation($this->_('Type'), 'TypeID', new \Pecee\UI\Form\Validate\ValidateInputInteger());
				$this->addInputValidation($this->_('Navn'), 'Name', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
				$this->addInputValidation($this->_('Unikt tag'), 'UniqueTag', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
		
				if(!$this->hasErrors()) {					
					$this->position->setSiteID($this->data->SiteID);
					$this->position->setTypeID($this->data->TypeID);
					$this->position->setUniqueTag($this->data->UniqueTag);
					$this->position->setName($this->data->Name);
					$this->position->setVisible($this->data->Visible);
					$this->position->setDisabled(!($this->data->Active));
					$this->position->setRules($this->rules);
					$this->position->update();
					
					Model_Ad_Log::Log(sprintf('%s oprettede en ny position SiteID: %s, Unikt tag: %s, Navn: %s',
					\Pecee\Model\User\ModelUser::Current()->getUsername(),
					$this->data->SiteID,
					$this->data->UniqueTag,
					$this->data->Name));
						
					\Pecee\Router::Redirect(\Pecee\Router::GetRoute('positions', ''));
				}
			}
		} else {
			$this->rules=$this->position->getRules()->getRows();
		}

	}
}