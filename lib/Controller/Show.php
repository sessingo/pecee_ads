<?php
class Controller_Show extends \Pecee\Controller\Controller {
	public function indexView() {
		//ob_start("ob_gzhandler");
		header('Content-type: application/javascript');
		//echo '/* Pecee Ads Administration - http://ads.pecee.dk */' . chr(10);
		try {
			echo new Widget_Ad_Show();
		} catch (Exception $e) {
			die(sprintf('/* An error occured:%s (%s) %s */', chr(10), $e->getCode(), $e->getMessage()) );
		}
	}
	public function frameView() {
		ob_start("ob_gzhandler");
		echo new Widget_Ad_Frame();
	}
}