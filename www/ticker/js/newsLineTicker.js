$(function(){
	var settings;
	var items;
	var animation;
	var c;
	var currentElement = 0;
	var previousElement = -1;
	$.NewsLineTicker = function( container, options ) {
		settings = {
		'timeout': 2000,
		'animationspeed': 200 };
		
		c = container;
		if( options )
			$.extend(settings, options);
		
		items = $(c).find('ul li');
		
		/* Do animation */
		$.NewsLineTicker.Animate();
	}

	$.NewsLineTicker.Animate = function() {
		if( currentElement >= items.length ) {
			currentElement = 0;
			previousElement = 0;
			clearTimeout(animation);
			$.NewsLineTicker.Animate();
		} else {
			items.each(function() {
				$(this).hide();
			})
			previousElement = (currentElement != 0) ? currentElement-1 : 0;

			if( previousElement != -1) {
				 $(items[previousElement]).fadeOut(settings.animationspeed);
				$(items[currentElement]).hide();
			}
			$(items[currentElement]).fadeIn(settings.animationspeed);
			
			
			currentElement++;
			
			animation = setTimeout( function() {
				$.NewsLineTicker.Animate();
			}, settings.timeout );
		}
	}
}); 