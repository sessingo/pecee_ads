<?php
class Dataset_Pools extends \Pecee\Dataset {
	public function __construct($choose=NULL, $siteId=NULL, $excludeIds=NULL) {
		if(!is_null($choose)) {
			$this->createArray('', $choose);
		}
		$pools=Model_Pool::Get(NULL, $siteId, NULL, NULL);
		if($pools && $pools->hasRows()) {
			foreach($pools->getRows() as $pool) {
				if(is_null($excludeIds) || !is_null($excludeIds) && !in_array($pool->getPoolID(), $excludeIds)) {
					$this->createArray($pool->getPoolID(), $pool->getName());
				}
			}
		}
	}
}