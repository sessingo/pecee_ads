<?php /* @var $this \Pecee\UI\Site */ ?>
<h3><?= $this->_('Sider')?></h3>
<ul class="menu">
	<li><a href="new/"><?= $this->_('Ny side')?></a></li>
	<li><a href="/index.php/sites/"><?= $this->_('Oversigt')?></a></li>
</ul>
<? if($this->sites->hasRows()) : ?>
<table cellspacing="0" width="100%" cellpadding="0" class="margin-top">
	<tr>
		<td width="20%" class="bold padding-bottom">
			<?= $this->_('Navn')?>
		</td>
		<td width="50%" class="bold padding-bottom">
			<?= $this->_('Url')?>
		</td>
		<td width="10%">
			<?= $this->_('Aktiv')?>
		</td>
		<td width="20%" align="right" class="bold padding-bottom">
			<?= $this->_('Funktioner'); ?>
		</td>
	</tr>	
<? foreach($this->sites->getRows() as $site) : ?>
	<tr> 
		<td height="25">
			<?= $site->getName(); ?>
		</td>
		<td>
			<a href="<?= $site->getUrl(); ?>" target="_blank"><?= $site->getUrl(); ?></a>
		</td>
		<td>
			<?= $this->form()->bool('disabled', !$site->getDisabled())->addAttribute('disabled','disabled'); ?>
		</td>
		<td align="right">
			<a href="<?= \Pecee\Router::GetRoute('sites', 'edit', array($site->SiteID)) ?>"><?= $this->_('Rediger') ?></a>
			<span class="separator">|</span>
			<a href="<?= \Pecee\Router::GetRoute('sites', 'refresh', array($site->SiteID)) ?>"><?= $this->_('Opdater') ?></a>
			<span class="separator">|</span>
			<a onclick="return confirm('<?= $this->_('Er du sikker på, at du vil slette denne side?');?>');" href="<?= \Pecee\Router::GetRoute('sites', 'delete', array($site->SiteID)) ?>"><?= $this->_('Slet')?></a>
		</td>
	</tr>
	<?php endforeach; ?>
</table>
<? endif; ?>