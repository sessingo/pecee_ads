<?php
class Widget_Sites extends Widget_Site {
	protected $sites;
	public function __construct() {
		$this->navActiveIndex=5;
		parent::__construct();
		$query = (isset($this->data->query)) ? $this->data->query : null;
		$this->sites = Model_Ad_Site::Get($query, NULL, 25, 0);
	}
	
}