<?php
class Controller_Default extends \Pecee\Controller\Controller {
	public function indexView() {
		echo new Widget_Home();
	}
	
	public function contactView( $SiteID = NULL, $PositionID = NULL ) {
		echo new Widget_Contact( $SiteID, $PositionID );
	}
	
	public function logView() {
		\Pecee\Auth::GetInstance()->setAuthLevel(2);
		echo new Widget_Log();
	}
	
	public function loginView() {
		echo new Widget_Login();
	}
	
	public function logoutView() {
		\Pecee\Model\User\ModelUser::Current()->signOut();
		\Pecee\Router::Redirect(\Pecee\Router::GetRoute('', ''));
	}
	
	public function clickView($adId) {
		if(\Pecee\Integer::is_int($adId)) {
			$ad=Model_Ad::GetById($adId);
			if($ad->hasRow()) {
				$ad->registerClick();
			}
			\Pecee\Router::Redirect($ad->getUrl());
		}
		die();
	}
}