<? /* @var $this Widget_Ad_Frame */ ?>
<?= $this->widget(new Widget_Ad_Single($this->siteId, $this->pools)); ?>
<? if($this->refreshRate > 0) : ?>
	<script type="text/javascript">
		window.onload=function() {
			setTimeout(function() {
				location.reload(true);
			}, <?= $this->refreshRate; ?>);
		};
	</script>
<? endif; ?>