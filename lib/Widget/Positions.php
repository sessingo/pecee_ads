<?php
class Widget_Positions extends Widget_Site {
	protected $positions;
	public function __construct() {
		$this->navActiveIndex=3;
		parent::__construct();
		$query = ($this->data->query) ? $this->data->query : null;
		$this->positions = Model_Ad_Position::Get($query, 0, 25);
	}
}