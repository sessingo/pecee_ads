<?php
class Widget_Users_Delete extends Widget_Site {
	public function __construct( $UserID ) {
		parent::__construct();
		if( \Pecee\Integer::is_int($UserID) ) {
			try {
				$user=\Pecee\Model\User\ModelUser::GetByUserID($UserID);
				if($user->hasRow()) {
					$user->delete();
				}
			} catch (Exception $e) {
				$this->setError($this->_('Du kan ikke slette denne bruger, da han har tilknyttede reklamer.'));
			}
			Model_Ad_Log::Log(sprintf('%s slettede brugeren %s', \Pecee\Model\User\ModelUser::Current()->getUsername(), $user->getUsername()));
		}
		\Pecee\Router::GoBack();
	}
}