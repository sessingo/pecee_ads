<?php /* @var $this Widget_Ads */ ?>

<div style="position:relative;">
	<div style="position:absolute;top:0px;right:0px;text-align:right;">
		<?= $this->form()->start('search', 'get')->addAttribute('style','padding-bottom:10px;')?>
		<?= $this->form()->input('siteId', 'hidden', $this->siteId) ?>
		<?= $this->form()->input('query', 'text', $this->getParam('query'))?>&nbsp;
		<?= $this->form()->submit('search', $this->_('Søg'))?>
		<?= $this->form()->end(); ?>
		
		<span style="font-size:11px;"><?= $this->_('Filtrer'); ?></span>&nbsp;
		<?= $this->form()->selectStart('filter', new Dataset_Sites($this->_('Alle')), $this->siteId)
			->addAttribute('onchange', 'top.location.href = \'?siteId=\' + this.value;') ?>
			
		<?= $this->form()->selectStart('poolId', new Dataset_Pools($this->_('Alle'), $this->siteId), $this->poolId)
			->addAttribute('onchange', 'top.location.href = \'?poolId=\' + this.value;') ?>
		
	</div>
	<h3><?= $this->_('Reklamer')?></h3>
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP) ?>
</div>
<? if($this->ads->hasRows()) : ?>
<table width="100%" class="margin-top">
	<tr>
		<td width="20%" class="bold padding-bottom">
			<?= $this->_('Navn')?>
		</td>
		<td width="8%" align="center" class="bold padding-bottom">
			<?= $this->_('Størrelse') ?>
		</td>
		<td width="20%" align="center" class="bold padding-bottom">
			<?= $this->_('Position')?>
		</td>
		<td width="8%" align="center" class="bold padding-bottom">
			%
		</td>
		<td width="8%" align="center" class="bold padding-bottom">
			<?= $this->_('Aktiv')?>
		</td>
		<td width="23%" class="bold padding-bottom" align="right">
			<?= $this->_('Funktioner') ?>
		</td>
	</tr>	
<? /* @var $ad Model_Ad */
	foreach($this->ads->getRows() as $ad) : ?>
	<tr> 
		<td class="padding-bottom">
			<div style="position:relative;">
				<div class="adholder" id="banner_placeholder_<?= $ad->AdID; ?>">
					<?= Helper::WriteAd($ad); ?>
				</div>
			</div>
			<a href="<?= \Pecee\Router::GetRoute(NULL, 'edit', array($ad->AdID)) ?>" onmouseover="showAd('<?= $ad->AdID ?>');" onmouseout="showAd('<?= $ad->AdID ?>');"><?= $ad->Name; ?></a>
		</td>
		<td align="center" height="25" class="padding-bottom">	
			
		</td>
		<td align="center" class="padding-bottom">
			
		</td>
		<td align="center" class="padding-bottom">
			<?= $ad->ShowPercentage?>%
		</td>
		<td align="center" class="padding-bottom">
			<?= $this->form()->input('active_' . $ad->AdID, 'checkbox')
			->addAttribute('disabled', 'true')
			->addAttribute(((!$ad->Disabled) ? 'checked' : ''), '');?>
		</td>
		<td align="right" class="padding-bottom">
			<a href="<?= \Pecee\Router::GetRoute(NULL, 'edit', array($ad->AdID)) ?>"><?= $this->_('Rediger') ?></a>
			<span class="separator">|</span>
			<a onclick="return confirm('<?= $this->_('Er du sikker på, at du vil slette denne reklame?');?>');" href="<?= \Pecee\Router::GetRoute(NULL, 'delete', array($ad->AdID)) ?>"><?= $this->_('Slet')?></a>
			<span class="separator">|</span>
			<a href="<?= \Pecee\Router::GetRoute(NULL, 'show', array($ad->AdID)) ?>"><?= $this->_('Vis')?></a>
			<span class="separator">|</span>
			<a href="<?= \Pecee\Router::GetRoute(NULL, 'statistic', array($ad->AdID)) ?>"><?= $this->_('Statistik')?></a>
		</td>
	</tr>
	<? endforeach; ?>
	<tr>
		<td colspan="7" width="100%" align="center" class="padding-top">
			<div class="padding-top margin-top large">
				<?= \Pecee\UI\Paging::DisplayNumberedPaging(6, $this->page, $this->ads->getMaxPages(), $this->pagingFormat, $this->_('Forrige'), $this->_('Næste'))?>
			</div>
		</td>
	</tr>
</table>
<? else: ?>
<div style="text-align:center;" class="bold large padding-top margin-top">
	<?= $this->_('Der er endnu ikke tilføjet nogle reklamer')?>
</div>
<? endif;?>