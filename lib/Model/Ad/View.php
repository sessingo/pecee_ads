<?php
class Model_Ad_View extends \Pecee\Model\Model {
	public function __construct() {
		parent::__construct('AdView', array('AdViewID' => NULL, 'AdID' => NULL, 'IPAddress' => '', 'Date' => \Pecee\Date::ToDateTime(time())));
	}
	
	public static function Find($adId, $ip) {
		return self::FetchOne('SELECT * FROM `AdView` WHERE `AdID` = %s AND IPAddress = %s', $adId, $ip);
	}
}