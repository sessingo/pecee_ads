<?php
class Model_Pool_Keyword extends \Pecee\Model\Model {
	public function __construct() {
		parent::__construct('PoolKeyword', array('PoolKeywordID' => NULL, 'PoolID' => NULL, 'Keyword' => NULL));
	}
	
	/**
	 * Get by pool id
	 * @param int $poolId
	 * @return Model_Pool_Keyword
	 */
	public static function GetByPoolId($poolId) {
		return self::FetchAll('SELECT * FROM `PoolKeyword` WHERE `PoolID` = %s', $poolId);
	}
	
	public static function Clear($poolId) {
		self::NonQuery('DELETE FROM `PoolKeyword` WHERE `PoolID` = %s', $poolId);
	}
}