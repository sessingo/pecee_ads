<?php
class Model_Ad_Log extends \Pecee\Model\Model { 
	public function __construct() {
		parent::__construct('Log', array('LogID' => NULL, 'Description' => '', 'Date' => \Pecee\Date::ToDateTime(time())));
	}
	
	public function getDate($unix=TRUE) {
		if($unix) {
			return strtotime($this->Date);
		}
		return $this->Date;
	}
	
	public static function Log($txt) {
		$log=new self();
		$log->setDescription($txt);
		$log->save();
		return $log;
	}
	
	public static function Get($query=NULL, $rows=10, $page=0) {
		$where = array('1=1');
		if($query) {
			$where[] = ' WHERE `Description` LIKE \'%'.\Pecee\DB\DB::Escape($query).'%\'';
		}
		return self::FetchPage('`LogID`', 'SELECT * FROM `Log` WHERE '.join(' AND ', $where) . ' ORDER BY `LogID` DESC', $rows, $page);
	}
}