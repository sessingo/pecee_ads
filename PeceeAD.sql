-- phpMyAdmin SQL Dump
-- version 3.3.7deb7
-- http://www.phpmyadmin.net
--
-- Vært: localhost
-- Genereringstid: 09. 12 2012 kl. 14:26:00
-- Serverversion: 5.1.63
-- PHP-version: 5.3.3-7+squeeze14

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `PeceeAD`
--

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Ad`
--

CREATE TABLE IF NOT EXISTS `Ad` (
  `AdID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` bigint(20) NOT NULL,
  `Name` varchar(300) NOT NULL,
  `Url` varchar(500) DEFAULT NULL,
  `Content` varchar(500) DEFAULT NULL,
  `ShowPercentage` int(3) NOT NULL,
  `Views` bigint(20) DEFAULT NULL,
  `UniqueViews` bigint(20) DEFAULT NULL,
  `Clicks` bigint(20) DEFAULT NULL,
  `UniqueClicks` bigint(20) DEFAULT NULL,
  `MaxViews` int(11) DEFAULT NULL,
  `IsHtml` tinyint(1) DEFAULT NULL,
  `Disabled` tinyint(1) DEFAULT NULL,
  `Deleted` tinyint(1) DEFAULT NULL,
  `CreatedDate` datetime NOT NULL,
  `ExpiresDate` datetime DEFAULT NULL,
  PRIMARY KEY (`AdID`),
  KEY `UserID` (`UserID`),
  KEY `ShowPercentage` (`ShowPercentage`),
  KEY `Views` (`Views`),
  KEY `UniqueViews` (`UniqueViews`),
  KEY `Clicks` (`Clicks`),
  KEY `UniqueClicks` (`UniqueClicks`),
  KEY `MaxViews` (`MaxViews`),
  KEY `IsHtml` (`IsHtml`),
  KEY `Disabled` (`Disabled`),
  KEY `Deleted` (`Deleted`),
  KEY `CreatedDate` (`CreatedDate`),
  KEY `ExpiresDate` (`ExpiresDate`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=77 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `AdClick`
--

CREATE TABLE IF NOT EXISTS `AdClick` (
  `AdClickID` bigint(20) NOT NULL AUTO_INCREMENT,
  `AdID` int(11) NOT NULL,
  `IPAddress` varchar(36) NOT NULL,
  `Date` datetime NOT NULL,
  PRIMARY KEY (`AdClickID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `AdPool`
--

CREATE TABLE IF NOT EXISTS `AdPool` (
  `AdPoolID` int(11) NOT NULL AUTO_INCREMENT,
  `AdID` int(11) NOT NULL,
  `PoolID` int(11) NOT NULL,
  PRIMARY KEY (`AdPoolID`),
  KEY `AdID` (`AdID`),
  KEY `PoolID` (`PoolID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=221 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `AdType`
--

CREATE TABLE IF NOT EXISTS `AdType` (
  `AdTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `Width` int(3) NOT NULL,
  `Height` int(3) NOT NULL,
  PRIMARY KEY (`AdTypeID`),
  KEY `Width` (`Width`),
  KEY `Height` (`Height`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `AdView`
--

CREATE TABLE IF NOT EXISTS `AdView` (
  `AdViewID` bigint(20) NOT NULL AUTO_INCREMENT,
  `AdID` int(11) NOT NULL,
  `IPAddress` varchar(36) NOT NULL,
  `Date` datetime NOT NULL,
  PRIMARY KEY (`AdViewID`),
  KEY `AdID` (`AdID`),
  KEY `Date` (`Date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=100272 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Cache`
--

CREATE TABLE IF NOT EXISTS `Cache` (
  `Key` varchar(400) NOT NULL,
  `Data` longtext NOT NULL,
  `ExpireDate` int(11) NOT NULL,
  PRIMARY KEY (`Key`),
  KEY `ExpireDate` (`ExpireDate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Language`
--

CREATE TABLE IF NOT EXISTS `Language` (
  `LanguageID` bigint(20) NOT NULL AUTO_INCREMENT,
  `OriginalText` longtext NOT NULL,
  `TranslatedText` longtext NOT NULL,
  `Locale` varchar(5) NOT NULL,
  `PageCode` varchar(32) DEFAULT NULL,
  `Path` varchar(350) NOT NULL,
  PRIMARY KEY (`LanguageID`),
  KEY `PageCode` (`PageCode`),
  KEY `Path` (`Path`),
  KEY `Locale` (`Locale`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Log`
--

CREATE TABLE IF NOT EXISTS `Log` (
  `LogID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Description` varchar(500) NOT NULL,
  `Date` datetime NOT NULL,
  PRIMARY KEY (`LogID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Pool`
--

CREATE TABLE IF NOT EXISTS `Pool` (
  `PoolID` int(11) NOT NULL AUTO_INCREMENT,
  `ParentPoolID` int(11) DEFAULT NULL,
  `PoolTypeID` int(11) NOT NULL,
  `SiteID` int(11) NOT NULL,
  `UserID` bigint(20) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Description` text,
  `RefreshRateMS` int(8) DEFAULT NULL,
  `Takeover` tinyint(1) DEFAULT NULL,
  `UseFrame` tinyint(1) DEFAULT NULL,
  `Disabled` tinyint(1) NOT NULL,
  `PubDate` datetime NOT NULL,
  `ActiveFrom` datetime DEFAULT NULL,
  `ActiveTo` datetime DEFAULT NULL,
  PRIMARY KEY (`PoolID`),
  KEY `ParentPoolID` (`ParentPoolID`),
  KEY `PoolTypeID` (`PoolTypeID`),
  KEY `SiteID` (`SiteID`),
  KEY `UserID` (`UserID`),
  KEY `Name` (`Name`),
  KEY `Disabled` (`Disabled`),
  KEY `PubDate` (`PubDate`),
  KEY `ActiveFrom` (`ActiveFrom`),
  KEY `ActiveTo` (`ActiveTo`),
  KEY `UseFrame` (`UseFrame`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `PoolKeyword`
--

CREATE TABLE IF NOT EXISTS `PoolKeyword` (
  `PoolKeywordID` int(11) NOT NULL AUTO_INCREMENT,
  `PoolID` int(11) NOT NULL,
  `Keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`PoolKeywordID`),
  KEY `PoolID` (`PoolID`),
  KEY `Keyword` (`Keyword`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `PoolType`
--

CREATE TABLE IF NOT EXISTS `PoolType` (
  `PoolTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `Width` int(3) NOT NULL,
  `Height` int(3) NOT NULL,
  PRIMARY KEY (`PoolTypeID`),
  KEY `Width` (`Width`),
  KEY `Height` (`Height`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Position`
--

CREATE TABLE IF NOT EXISTS `Position` (
  `PositionID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteID` int(11) NOT NULL,
  `TypeID` int(11) NOT NULL,
  `UniqueTag` varchar(255) NOT NULL,
  `Name` varchar(300) NOT NULL,
  `Visible` tinyint(1) DEFAULT NULL,
  `Disabled` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`PositionID`),
  KEY `SiteID` (`SiteID`),
  KEY `TypeID` (`TypeID`),
  KEY `UniqueTag` (`UniqueTag`),
  KEY `Visible` (`Visible`),
  KEY `Disabled` (`Disabled`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `PositionRule`
--

CREATE TABLE IF NOT EXISTS `PositionRule` (
  `PositionRuleID` int(11) NOT NULL AUTO_INCREMENT,
  `PositionID` int(11) NOT NULL,
  `Rule` varchar(500) NOT NULL,
  `Allowed` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`PositionRuleID`),
  KEY `PositionID` (`PositionID`),
  KEY `Rule` (`Rule`),
  KEY `Match` (`Allowed`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Rewrite`
--

CREATE TABLE IF NOT EXISTS `Rewrite` (
  `OriginalPath` varchar(500) NOT NULL,
  `RewritePath` varchar(500) NOT NULL,
  PRIMARY KEY (`OriginalPath`),
  KEY `RewritePath` (`RewritePath`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Site`
--

CREATE TABLE IF NOT EXISTS `Site` (
  `SiteID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(300) NOT NULL,
  `Url` varchar(500) NOT NULL,
  `Visible` tinyint(1) NOT NULL,
  `Disabled` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`SiteID`),
  KEY `Url` (`Url`),
  KEY `Visible` (`Visible`),
  KEY `Disabled` (`Disabled`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Template`
--

CREATE TABLE IF NOT EXISTS `Template` (
  `TemplateID` int(11) NOT NULL AUTO_INCREMENT,
  `PoolID` int(11) NOT NULL,
  `Name` varchar(300) NOT NULL,
  `Content` varchar(1024) NOT NULL,
  `PubDate` datetime NOT NULL,
  `Disabled` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`TemplateID`),
  KEY `PoolID` (`PoolID`),
  KEY `PubDate` (`PubDate`),
  KEY `Disabled` (`Disabled`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `User`
--

CREATE TABLE IF NOT EXISTS `User` (
  `UserID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Username` varchar(300) NOT NULL,
  `Password` varchar(32) NOT NULL,
  `LastActivity` datetime DEFAULT NULL,
  `AdminLevel` tinyint(1) NOT NULL,
  `Deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`UserID`),
  KEY `Username` (`Username`),
  KEY `Password` (`Password`),
  KEY `AdminLevel` (`AdminLevel`),
  KEY `Deleted` (`Deleted`),
  KEY `LastActivity` (`LastActivity`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `UserData`
--

CREATE TABLE IF NOT EXISTS `UserData` (
  `UserID` bigint(20) NOT NULL,
  `Key` varchar(255) NOT NULL,
  `Value` text NOT NULL,
  KEY `UserID` (`UserID`),
  KEY `Key` (`Key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `UserIncorrectLogin`
--

CREATE TABLE IF NOT EXISTS `UserIncorrectLogin` (
  `Username` varchar(300) NOT NULL,
  `RequestTime` int(11) NOT NULL,
  `IPAddress` varchar(32) NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT '1',
  KEY `Username` (`Username`),
  KEY `RequestTime` (`RequestTime`),
  KEY `IPAddress` (`IPAddress`),
  KEY `Active` (`Active`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `UserPasswordReset`
--

CREATE TABLE IF NOT EXISTS `UserPasswordReset` (
  `UserID` bigint(20) NOT NULL,
  `Key` varchar(32) NOT NULL,
  `Date` int(11) NOT NULL,
  KEY `UserID` (`UserID`),
  KEY `Key` (`Key`),
  KEY `Date` (`Date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Begrænsninger for dumpede tabeller
--

--
-- Begrænsninger for tabel `PositionRule`
--
ALTER TABLE `PositionRule`
  ADD CONSTRAINT `FK_PositionRule_1` FOREIGN KEY (`PositionID`) REFERENCES `Position` (`PositionID`);
