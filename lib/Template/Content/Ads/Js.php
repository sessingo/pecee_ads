<?php /* @var $this Widget_Ads_Js */ ?>
<h3><?= $this->_('Generer javascript')?></h3>
<div style="position:relative;">
	<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP) ?>
	<div style="position:absolute;top:0px;right:0px;">
		<a href="<?= \Pecee\Router::GoBack(true); ?>"><?= $this->_('Gå tilbage')?></a>
	</div>
</div>

<?= $this->form()->start('generateJS', 'post', '') ?>
<?= $this->form()->input('autoPostBack', 'hidden', '0')->addAttribute('ID', 'autoPostBack')?>

<table cellspacing="0" cellpadding="0" width="100%">

	<tr>
		<td width="30%" class="padding-top">
			<?= $this->_('Site') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->selectStart('SiteID', new Dataset_Sites($this->_('-- Vælg --')), $this->siteId)
			->addAttribute('onchange', 'top.location = \'' . \Pecee\Router::GetRoute('ads', 'js') . '\' + this.value + \'\';');
			?>
		</td>
	</tr>
	<? if($this->pools && $this->pools->hasRows()) : ?>
	<tr>
		<td width="30%" class="padding-top padding-bottom" valign="top">
			<?= $this->_('Pools') ?>
		</td>
		<td class="padding-top padding-bottom js-checkbox">
			<? foreach($this->pools->getRows() as $pool) : ?>
				<div>
					<?= $this->form()->input('pool[]', 'checkbox', $pool->getPoolID())->addAttribute('ID', 'pool_'.$pool->getPoolID()); ?> 
					<?= $this->form()->label($pool->getName(), 'pool_'.$pool->getPoolID())?>
				</div>
			<? endforeach; ?>
		</td>
	</tr>
	<tr>
		<td width="30%" class="padding-top padding-bottom" valign="top">
			<?= $this->_('Keywords (seperer med komma)') ?>
		</td>
		<td class="padding-top padding-bottom js-checkbox">
			<?= $this->form()->input('keywords', 'text')->addAttribute('style','width:300px;')->addAttribute('id','keywords')?>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="padding-top">
			<div class="padding-top padding-bottom">
				<?= $this->_('Indsæt denne javascript kode på din hjemmeside')?>
			</div>
			<?= $this->form()->textarea('javascript', 5, 90, '')->addAttribute('id', 'output'); ?>
			<script type="text/javascript">
				$(document).ready(function() {
					var pools=[];
					var keywords='';

					var generate=function() {
						var o='<?= sprintf('http://%s/ad?site=%s&pools=', $_SERVER['HTTP_HOST'], $this->siteId) ?>' + pools.join(',');
						if(keywords != '') {
							o+='&keywords='+keywords;
						}
						$('#output').val('<scr'+'ipt type="text/javascript" src="'+o+'"></scr'+'ipt>');
					};
					
					$('.js-checkbox input[type="checkbox"]').live('click', function() {
						pools=[];
						$('.js-checkbox input[type="checkbox"]:checked').each(function() {
							pools.push($(this).val());
						});
						generate();
					});

					$('#keywords').bind('keyup', function() {
						keywords=$(this).val();
						generate();
					});
				});
			</script>
		</td>
	</tr>
	<? endif;?>
</table>

<?= $this->form()->end(); ?>