<?php
class Widget_Ads_Edit extends Widget_Site {
	protected $ad;
	protected $pools;
	protected $poolIdsToExclude;
	protected $siteId;
	protected $isHtml;
	
	public function __construct($adId) {
		$this->navActiveIndex=1;
		parent::__construct();
		
		if($adId) {
			$this->ad = Model_Ad::GetById($adId);
			
			$this->poolIdsToExclude=array();
			
			if($this->isPostBack()) {
				$this->isHtml=$this->data->isHtml;
				$this->pools=\Pecee\String\Encoding::Base64Decode($this->data->pools, array());
					
				if( $this->data->autoPostBack != '1' ) {
					$this->addInputValidation($this->_('Navn'), 'name', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
					if(!$this->isHtml) {
						$this->addInputValidation($this->_('Klik url'), 'url', new \Pecee\UI\Form\Validate\ValidateInputUri());
					}
			
					$this->addInputValidation($this->_('Udløbs dato'), 'expiresDate', new \Pecee\UI\Form\Validate\ValidateInputDate(TRUE));
					$this->addInputValidation($this->_('Indhold'), 'content', (($this->isHtml) ? new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty() : new \Pecee\UI\Form\Validate\ValidateInputUri()));
					$this->addInputValidation($this->_('Visnings procent'), 'showPercentage', new \Pecee\UI\Form\Validate\ValidateInputInteger());
						
					if(!$this->hasErrors()) {
						$this->ad->UserID=\Pecee\Model\User\ModelUser::Current()->getUserID();
						$this->ad->Name=$this->data->name;
						$this->ad->Url=$this->data->url;
						$this->ad->Content=$this->data->content;
						$this->ad->ShowPercentage=$this->data->showPercentage;
						$this->ad->IsHtml=$this->isHtml;
						$this->ad->ExpiresDate=($this->data->expiresDate) ? \Pecee\Date::FromToDateTime($this->data->expiresDate) : NULL;
						$this->ad->MaxViews=($this->data->maxViews) ?$this->data->MaxViews : NULL;
						$this->ad->Disabled=(!$this->data->active);
						$this->ad->setPools($this->pools);
						$this->ad->update();
						
						$this->ad->clearCache();
							
						Model_Ad_Log::Log(sprintf('%s redigerede reklamen: %s (%s)', \Pecee\Model\User\ModelUser::Current()->getUsername(), $this->data->Name, $adId));
						\Pecee\Router::Redirect(\Pecee\Router::GetRoute('ads', ''));
					}
				} else {
					if($this->data->addPool && $this->data->poolId) {
						$this->pools[]=Model_Pool::GetById($this->data->poolId);
					} elseif(\Pecee\Integer::is_int($this->data->poolToDelete)) {
						unset($this->pools[$this->data->poolToDelete]);
					}
				}
					
				$this->setExcludedPoolIds();
			} else {
				$this->isHtml=$this->ad->getIsHtml();
				$this->pools=$this->ad->getPools();
				$this->setExcludedPoolIds();
			}
			
		} else {
			\Pecee\Router::Redirect(\Pecee\Router::GetRoute('ads', ''));
		}
	}
	
	protected function setExcludedPoolIds() {
		if(count($this->pools) > 0) {
			foreach($this->pools as $pool) {
				if($pool instanceof Model_Pool) {
					if(!in_array($pool->getPoolID(), $this->poolIdsToExclude)) {
						$this->poolIdsToExclude[]=$pool->getPoolID();
					}
				}
			}
		}
	}
}