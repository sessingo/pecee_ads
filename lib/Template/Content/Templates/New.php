<?php /* @var $this Widget_Templates_New */ ?>
<h3><?= $this->_('Ny template')?></h3>
<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP) ?>
<?= $this->showErrors(); ?>
<?= $this->form()->start('newTemplate', 'post', '') ?>
<?= $this->form()->input('template', 'hidden', \Pecee\String\Encoding::Base64Encode($this->template)); ?>
<?= $this->form()->input('autoPostBack', 'hidden', '0')->addAttribute('ID', 'autoPostBack')?>
<?= $this->form()->input('previewPostBack', 'hidden', '0')->addAttribute('ID', 'previewPostBack') ?>

<table cellspacing="0" cellpadding="0" width="100%" class="margin-top">
	<tr>
		<td width="30%">
			<?= $this->_('Navn') ?>
		</td>
		<td>
			<?= $this->form()->input('Name', 'text'); ?>
		</td>
	</tr>
	<tr>
		<td width="30%" class="padding-top" valign="top">
			<?= $this->_('Javascript') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->textarea('content', 20, 70)?>
			<table cellspacing="0" cellpadding="0" class="padding-top padding-bottom">
				<tr>
					<td width="23%">
						<code><a href="javascript:;" onclick="insertTag('%ADCONTENT%');">%ADCONTENT%</a></code> <br/>
						<code><a href="javascript:;" onclick="insertTag('%UNIQUETAG%');">%UNIQUETAG%</a></code> <br/>
						<code><a href="javascript:;" onclick="insertTag('%POOLID%');">%POOLID%</a></code> <br/>
						<code><a href="javascript:;" onclick="insertTag('%SITEID%');">%SITEID%</a></code> <br/>
						<code><a href="javascript:;" onclick="insertTag('%TYPEID%');">%TYPEID%</a></code> <br/>
						<code><a href="javascript:;" onclick="insertTag('%TYPEWIDTH%');">%TYPEWIDTH%</a></code> <br/>
						<code><a href="javascript:;" onclick="insertTag('%TYPEHEIGHT%');">%TYPEHEIGHT%</a></code>
					</td>
					<td>
						<span class="separator">|</span> 
						<?= $this->_('Indsætter banner')?><br/>
						<span class="separator">|</span> 
						<?= $this->_('Indsætter unikt tag')?><br/>
						<span class="separator">|</span> 
						<?= $this->_('Indsætter positions-id\'et')?><br/>
						<span class="separator">|</span> 
						<?= $this->_('Indsætter side id\'et')?><br/>
						<span class="separator">|</span> 
						<?= $this->_('Indsætter type id\'et')?><br/>
						<span class="separator">|</span> 
						<?= $this->_('Indsætter type bredden')?><br/>
						<span class="separator">|</span> 
						<?= $this->_('Indsætter type højden')?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<h3>Eksempel</h3>
		</td>
	</tr>
	<tr>
		<td valign="top">
			<?= $this->_('Side'); ?>
		</td>
		<td>
			<?= $this->form()->selectStart('SiteID', new Dataset_Sites($this->_('-- Vælg --')))
			->addAttribute('onchange', 'document.newTemplate.action = \'' . \Pecee\Router::GetRoute('templates', 'new') . '\' + this.value + \'/\';document.getElementById(\'autoPostBack\').value = \'1\';document.newTemplate.submit();'); ?>
		</td>
	</tr>
	<tr>
		<td class="padding-top padding-bottom" valign="top">
			<?= $this->_('Position') ?>
		</td>
		<td class="padding-top padding-bottom">
			<?= $this->form()->selectStart('PoolID', new Dataset_Pools($this->_('-- Vælg --'), $this->siteId))
				->addAttribute(((!$this->siteId) ? 'disabled' : ''), '')
				->addAttribute('onchange', 'document.getElementById(\'previewPostBack\').value = \'1\';form.submit();')
				->addAttribute('ID', 'PoolID') ?>
			<div class="padding-top">
				<?= $this->form()->input('preview', 'button', $this->_('Vis eksempel'))
				->addAttribute('class', 'margin-top')
				->addAttribute(((!$this->showPreviewButton) ? 'disabled' : ''), '')
				->addAttribute('onclick', 'document.getElementById(\'PoolID\').selectedIndex = 0;this.disabled=true;window.open(\''.
								\Pecee\Router::GetRoute('templates', 'preview', NULL, array('template' => urlencode(\Pecee\String\Encoding::Base64Encode($this->template)))) .'\',\'_blank\');'); ?>
			</div>
		</td>
	</tr>
	<tr>
		<td>
		</td>
		<td class="padding-top">
			<?= $this->form()->submit('SaveSubmit', $this->_('Gem ændringer'))
			->addAttribute('class', 'margin-top')?>
			<?= $this->form()->input('cancel', 'button', $this->_('Annuller'))
				->addAttribute('onclick', 'top.location.href=\''.\Pecee\Router::GetRoute('templates', '').'\';')
				->addAttribute('style', 'margin-left:15px;');
			?>
		</td>
	</tr>
</table>
<?= $this->form()->end(); ?>
<script type="text/javascript"> 
	function insertTag(tag) { 
		v = document.newTemplate.newTemplate_content; v.value += tag;
	}
</script>