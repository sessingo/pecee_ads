<? /* @var $this Widget_Users */ ?>
<h3><?= $this->_('Brugere')?></h3>
<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP) ?>

<? if( $this->users->hasRows() ): ?>
<table cellspacing="0" cellpadding="0" width="100%" class="margin-top">
	<tr>
		<td width="40%" class="bold padding-bottom">
			Brugernavn
		</td>
		<td width="30%" class="bold padding-bottom">
			Brugertype
		</td>
		<td align="right" class="bold padding-bottom">
			Funktioner
		</td>
	</tr>
	<? foreach($this->users->getRows() as $user) : ?>
	<tr>
		<td height="25">
			<?= $user->getUsername();?>
		</td>
		<td>
			<?= $this->getAdminTitle($user->getAdminLevel()) ?>
		</td>
		<td align="right" class="padding-top">
			<a href="<?= \Pecee\Router::GetRoute('users', 'edit', array($user->getUserID())); ?>"><?= $this->_('Rediger')?></a>
			<span class="separator">|</span>
			<a href="<?= \Pecee\Router::GetRoute('users', 'delete', array($user->getUserID())) ?>"><?= $this->_('Slet') ?></a>
		</td>
	</tr>
	<? endforeach; ?>
</table>
<? endif; ?>