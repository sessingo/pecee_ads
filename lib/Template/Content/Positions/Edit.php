<?php /* @var $this Widget_Positions_Edit */ ?>
<h3><?= $this->_('Rediger position')?></h3>
<?= $this->getSite()->LOCATION(\Pecee\UI\Site::LOCATION_TOP) ?>
<?= $this->form()->start('editPosition')?>
<?= $this->form()->input('data', 'hidden', \Pecee\String\Encoding::Base64Encode($this->rules)); ?>
<?= $this->showErrors('editPosition'); ?>

<table cellspacing="0" cellpadding="0" width="100%" class="margin-top">
	<tr>
		<td width="30%">
			<?= $this->_('Navn') ?>
		</td>
		<td>
			<?= $this->form()->input('Name', 'text', $this->position->getName()); ?>
		</td>
	</tr>
	<tr>
		<td width="30%" class="padding-top">
			<?= $this->_('Side') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->selectStart('SiteID', new Dataset_Sites($this->_('-- Vælg --')), $this->position->getSiteID())?>
		</td>
	</tr>
	<tr>
		<td width="30%" class="padding-top">
			<?= $this->_('Type') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->selectStart('TypeID', new Dataset_Types(), $this->position->getPoolTypeID()); ?>
		</td>
	</tr>
	<tr>
		<td width="30%" class="padding-top">
			<?= $this->_('Unikt tag') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->input('UniqueTag', 'text', $this->position->getUniqueTag()); ?>
		</td>
	</tr>
	<tr>
		<td width="30%" class="padding-top">
			<?= $this->_('Synlig') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->bool('Visible', $this->position->getVisible()); ?>
		</td>
	</tr>
	<tr>
		<td width="30%" class="padding-top">
			<?= $this->_('Aktiv') ?>
		</td>
		<td class="padding-top">
			<?= $this->form()->bool('Active', !($this->position->getDisabled())); ?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" class="padding-top">
			<?= $this->_('Regler')?>
		</td>
		<td class="padding-top padding-bottom" valign="top" style="border-bottom:1px solid #CCC;">
			<? if($this->rules) : ?>
				<?= $this->form()->input('deleteRule', 'hidden', '')->addAttribute('ID', 'deleteRule')?>
				<table cellspacing="0" cellpadding="0" width="100%" class="padding-bottom">
					<? foreach($this->rules as $key=>$rule): ?>
						<tr>
							<td height="25">
							<?= $this->_('Hvis stien matcher på') . ' <code>' . $rule->Rule . '</code> ' ?>
							<?= ($rule->getAllowed()) ? 'tillad' : 'tillad ikke'; ?>
							</td>
							<td align="right">
								<a href="javascript:;" onclick="document.getElementById('deleteRule').value = '<?= $key ?>';document.editPosition.submit();"><?= $this->_('Slet')?></a>
							</td>
						</tr>
					<? endforeach; ?>
				</table>
			<? else: ?>
				<?= $this->_('Ingen regler tilføjet')?>
			<? endif; ?>
			
			<div class="margin-bottom" style="text-align:right;">
				<?= $this->form()->input('addNewRule', 'hidden', '0')->addAttribute('ID', 'addNewRule')?>
				<?= $this->_('URL'); ?>&nbsp;
				<?= $this->form()->input('Rule', 'text', ''); ?>
				<span class="separator">&nbsp;</span>
				<?= $this->form()->bool('RuleAllowed', TRUE, FALSE)->addAttribute('ID', 'RuleAllowed') ?>
				<?= $this->form()->label($this->_('Tillad'), 'RuleAllowed')?>
				<span class="separator">&nbsp;</span>
				<?= $this->form()->input('addRule', 'button', $this->_('Tilføj regel'))->addAttribute('onclick', 'document.getElementById(\'addNewRule\').value = \'1\';document.editPosition.submit();') ?>
			</div>
		</td>
	</tr>
	<tr>
		<td width="30%">
		</td>
		<td class="padding-top">
			<?= $this->form()->submit('submit', $this->_('Gem'))->addAttribute('class', 'margin-top'); ?>
		</td>
	</tr>
</table>
<?= $this->form()->end();?>