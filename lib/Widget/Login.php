<?php
class Widget_Login extends Widget_Site {
	public function __construct() {
		parent::__construct(NULL);
		
		$this->addInputValidation($this->_('Brugernavn'), 'username', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
		$this->addInputValidation($this->_('Adgangskode'), 'password', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
		
		if( $this->isPostBack() && !$this->hasErrors()  ) {
			$user=\Pecee\Model\User\ModelUser::Authenticate($this->data->username, $this->data->password, $this->data->rememberInformations);
			if(!($user instanceof \Pecee\Model\User\ModelUser)) {
				$this->setError($this->_('Ugyldig kombination af brugernavn og/eller adgangskode.'));
			}
			\Pecee\Router::Redirect( \Pecee\Router::GetRoute('', '') );
		}
	}
}