<?php /* @var $this Widget_Login */ ?>

<h3><?= $this->_('Log ind')?></h3>
<?= $this->form()->start('login') ?>
<?= $this->showErrors('login'); ?>
<table cellspacing="5" cellpadding="0">
	<tr>
		<td width="130">
			<?= $this->_('Brugernavn') ?>
		</td>
		<td>
			<?= $this->form()->input('username', 'text') ?>
		</td>
	</tr>
	<tr>
		<td>
			<?= $this->_('Adgangskode') ?>
		</td>
		<td>
			<?= $this->form()->input('password', 'password') ?>
		</td>
	</tr>
	<tr>
		<td>
		</td>
		<td class="padding-top">
			<?= $this->form()->input('rememberInformations', 'checkbox', '1')
				->addAttribute('ID', 'remember')
				->addAttribute('style', 'margin-right:10px;')?>
			<?= $this->form()->label($this->_('Husk mine informationer'), 'remember')?>
		</td>
	</tr>
	<tr>
		<td>
		</td>
		<td height="50">
			<?= $this->form()->submit('submit', $this->_('Log ind')) ?>
		</td>
	</tr>
</table>
<?= $this->form()->end(); ?>
