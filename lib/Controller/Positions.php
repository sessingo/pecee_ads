<?php
class Controller_Positions extends \Pecee\Controller\Controller {
	
	private function ShowMenu() {
		$menu = new \Pecee\UI\Menu\Menu();
		$menu->setClass('menu');
		$menu->addItem($this->_('Ny position'), \Pecee\Router::GetRoute('positions', 'new'));
		$menu->addItem($this->_('Oversigt'), \Pecee\Router::GetRoute('positions', ''));
		\Pecee\UI\Site::GetInstance()->addToLocation($menu, \Pecee\UI\Site::LOCATION_TOP);
	}
	
	public function indexView() {
		$this->ShowMenu();
		$widget = new Widget_Positions();
		echo $widget;
	}
	
	public function newView() {
		$this->ShowMenu();
		$widget = new Widget_Positions_New();
		echo $widget;
	}
	
	public function editView($PositionID) {
		$this->ShowMenu();
		$widget = new Widget_Positions_Edit($PositionID);
		echo $widget;
	}
	
	public function deleteView($PositionID) {
		$this->ShowMenu();
		$widget = new Widget_Positions_Delete($PositionID);
		echo $widget;
	}
	
}