<?php
class Widget_Pools_New extends Widget_Site {
	protected $keywords;
	public function __construct() {
		$this->navActiveIndex=3;
		parent::__construct();
		
		if($this->isPostBack()) {
			$this->isHtml=$this->data->isHtml;
			$this->keywords=\Pecee\String\Encoding::Base64Decode($this->data->keywords, array());
			if($this->data->autoPostBack != '1') {
				if($this->isPostBack()) {
					$this->addInputValidation($this->_('Navn'), 'name', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
					$this->addInputValidation($this->_('Type'), 'typeId', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
					$this->addInputValidation($this->_('Side'), 'siteId', new \Pecee\UI\Form\Validate\ValidateInputNotNullOrEmpty());
					$this->addInputValidation($this->_('Aktiv fra'), 'activeFrom', new \Pecee\UI\Form\Validate\ValidateInputDate(TRUE));
					$this->addInputValidation($this->_('Aktiv til'), 'activeTo', new \Pecee\UI\Form\Validate\ValidateInputDate(TRUE));
						
					if(!$this->hasErrors()) {
						$pool=new Model_Pool();
						$pool->Name=$this->data->name;
						$pool->Description=$this->data->description;
						$pool->PoolTypeID=$this->data->typeId;
						$pool->SiteID=$this->data->siteId;
						$pool->ParentPoolID=$this->data->ParentPoolID;
						$pool->ActiveFrom=($this->data->activeFrom) ? \Pecee\Date::ToDateTime(strtotime($this->data->activeFrom)) : NULL;
						$pool->ActiveTo=($this->data->activeTo) ? \Pecee\Date::ToDateTime(strtotime($this->data->activeTo)) : NULL;
						$pool->RefreshRateMS=($this->data->refreshInterval) ? $this->data->refreshInterval : NULL;
						$pool->Takeover=$this->data->takeover;
						$pool->UseFrame=($this->data->useFrame);
						$pool->Disabled=(!$this->data->disabled);
						$pool->setKeywords($this->keywords);
						$pool->save();
				
						\Pecee\Router::Redirect(\Pecee\Router::GetRoute(NULL, ''));
					}
				}
			} else {
				if($this->data->addKeyword && $this->data->keyword) {
					$this->keywords[]=$this->data->keyword;
				} elseif(\Pecee\Integer::is_int($this->data->keywordToDelete)) {
					unset($this->keywords[$this->data->keywordToDelete]);
				}
			}
		}
	}
}