<?php
class Widget_Sites_Delete extends Widget_Site {
	public function __construct($siteId) {
		parent::__construct();
		if(\Pecee\Integer::is_int($siteId) ) {
			$site=Model_Ad_Site::GetById($siteId);
			if($site->hasRow()) {
				$site->delete();
				Model_Ad_Log::Log(sprintf('%s slettede sitet %s ()', 
									\Pecee\Model\User\ModelUser::Current()->getUserID(), 
									$site->getName(), 
									$siteId));
			}
			
		}
		\Pecee\Router::GoBack();
	}
}