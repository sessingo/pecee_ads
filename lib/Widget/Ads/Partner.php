<?php
class Widget_Ads_Partner extends Widget_Site {
	protected $ads;
	protected $query;
	protected $siteId;
	protected $positionId;
	protected $page;
	
	public function __construct() {
		$this->navActiveIndex=1;
		parent::__construct(1);
		$this->query = $this->getParam('query');
		$this->siteId = $this->getParam('SiteID');
		$this->positionId = $this->getParam('PositionID');
		$this->page = $this->getParam('Page',0);
		
		$this->ads = Model_Ad::Get(\Pecee\Model\User\ModelUser::Current()->getUserID(), $this->query, $this->siteId, $this->positionId, 15, $this->page);
	}
}