$(function(){ 
		var currentElement = -1;
		var lastElement = -1;
		var ticker;

		var headlines;
		var thumbnails;
		var pictures;
		var settings;
		$.newsTicker = function(container, options) {
			settings = {
				'timeout': 2000,
			'animationtype': 'fade',
			'animationspeed': 250,
			'currentclass': 'current' };

		if(options) {
			$.extend(settings, options);
		}
		
		headlines = $(container).find('ul#headlines li');
		thumbnails = $(container).find('ul#thumbnails li');
		pictures = $(container).find('ul#pictures li');
		
		if( pictures.length > 0 && headlines.length > 0 && thumbnails.length > 0 ) {
			$(container).css('position', 'relative');
			/* Hide all elements */
			for(i=0;i<pictures.length;i++) {
				$(pictures[i]).css('z-index', String(pictures.length-i)).css('position', 'absolute').hide();
			}
			for(i=0;i<headlines.length;i++) {
				$(headlines[i]).hide();
			}
			/* Add events on thumbnails */
			thumbnails.each(function(i) {
				$(this).click( function() {
					if( currentElement != i ) {
						clearTimeout(ticker);
						lastElement = currentElement;
						currentElement = i;
						$.newsTicker.show(i);
					}
					
				} );
			});
			$.newsTicker.next(-1);
		}
	}

	$.newsTicker.next = function(current) {	
		next = ( (current+1) >= pictures.length ) ? 0 : (current + 1);
		lastElement = current;
		$.newsTicker.show(next);
		ticker = setTimeout(function() {
			$.newsTicker.next(next);
		}, settings.timeout);
	}

	$.newsTicker.show = function( index ) {
		if( headlines.length > index && thumbnails.length > index && pictures.length > index ) {
			currentElement = index;
			/* Remove existing *current* classes */
			$(thumbnails).find('a.' + settings.currentclass).each(function() {
				
				$(this).removeClass(settings.currentclass);
			});
			
			$(pictures).find(settings.currentclass).each(function() {
				$(this).removeClass(settings.currentclass);
			});

			$(headlines).find(settings.currentclass).each(function() {
				$(this).removeClass(settings.currentclass);
			});

			/* Set items active and add current class */
			var hide = ( lastElement != -1 && lastElement < pictures.length ) ? true : false;
			if( hide ) {	
				if( settings.animationtype == 'slide' ) {
					$(pictures[index]).addClass(settings.currentclass).slide(settings.animationspeed);
					if( hide ) {
						$(pictures[lastElement]).slide();
					}
				} else {
					$(pictures[index]).addClass(settings.currentclass).fadeIn(settings.animationspeed);
					if( hide ) {
						$(pictures[lastElement]).addClass(settings.currentclass).fadeOut(settings.animationspeed);
					}
				}
				/* To get a smooth fade/text animation */		
				setTimeout( function() {
					$(headlines[lastElement]).addClass(settings.currentclass).hide();
					$(headlines[index]).addClass(settings.currentclass).show();
					$(thumbnails[index]).find('a').addClass(settings.currentclass);
				}, 90 );
			} else {
				$(pictures[index]).addClass(settings.currentclass).show();
				$(headlines[index]).addClass(settings.currentclass).show();
				$(thumbnails[index]).find('a').addClass(settings.currentclass);
			}
		} 
	}
	
});